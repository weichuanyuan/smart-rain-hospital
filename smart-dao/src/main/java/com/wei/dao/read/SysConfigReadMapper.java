package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.SysConfig;

public interface SysConfigReadMapper extends IBaseReadDao<SysConfig,Integer> {
    SysConfig getBySysCode(String sysCode);
}