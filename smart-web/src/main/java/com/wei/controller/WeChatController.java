package com.wei.controller;

import com.wei.service.IWechatAuthService;
import com.wei.web.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@Controller
@RequestMapping("/weChat")
@Api(value = "/weChatController", tags = "微信相关api")
public class WeChatController {

    @Autowired
    private IWechatAuthService wechatAuthService;

    @ResponseBody
    @GetMapping("/login")
    @ApiOperation("微信使用code和appid换取用户信息")
    public ResultVo login(String code, String appid, String secret) {
        return new ResultVo("200","生成成功",wechatAuthService.getToken(code,appid,secret));
    }
    @ResponseBody
    @PostMapping("/sendVerifyCode")
    @ApiOperation("短信验证码")
    public  ResultVo sendVerifyCode(@RequestBody Map<String,String> map){
        return new ResultVo("200","生成成功",wechatAuthService.sendVerifyCode(map.get("phoneNumber")));
    }
}
