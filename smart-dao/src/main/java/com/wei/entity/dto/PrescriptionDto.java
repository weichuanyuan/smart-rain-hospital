package com.wei.entity.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class PrescriptionDto {
    /**
     * 处方号
     */
    private Integer prescriptionId;

    /**
     * 处方时间
     */
    private Date prescriptionTime;
    /**
     * 开单科室
     */
    private String prescriptionDeptName;
    /**
     * 开单医生
     */
    private String prescriptionDrname;
    /**
     * 处方总金额
     */
    private BigDecimal chargeFee;
    /**
     * 处方类型
     */
    private String prescriptionTypeName;

    private List<ChargeDto> chargeDtoList;
    private Integer mainId;

    public Integer getMainId() {
        return mainId;
    }

    public void setMainId(Integer mainId) {
        this.mainId = mainId;
    }

    public List<ChargeDto> getChargeDtoList() {
        return chargeDtoList;
    }

    public void setChargeDtoList(List<ChargeDto> chargeDtoList) {
        this.chargeDtoList = chargeDtoList;
    }

    /**
     * 指引地址
     */
    private String guideAddress;

    public Integer getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(Integer prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public Date getPrescriptionTime() {
        return prescriptionTime;
    }

    public void setPrescriptionTime(Date prescriptionTime) {
        this.prescriptionTime = prescriptionTime;
    }

    public String getPrescriptionDeptName() {
        return prescriptionDeptName;
    }

    public void setPrescriptionDeptName(String prescriptionDeptName) {
        this.prescriptionDeptName = prescriptionDeptName;
    }

    public String getPrescriptionDrname() {
        return prescriptionDrname;
    }

    public void setPrescriptionDrname(String prescriptionDrname) {
        this.prescriptionDrname = prescriptionDrname;
    }

    public BigDecimal getChargeFee() {
        return chargeFee;
    }

    public void setChargeFee(BigDecimal chargeFee) {
        this.chargeFee = chargeFee;
    }

    public String getPrescriptionTypeName() {
        return prescriptionTypeName;
    }

    public void setPrescriptionTypeName(String prescriptionTypeName) {
        this.prescriptionTypeName = prescriptionTypeName;
    }

    public String getGuideAddress() {
        return guideAddress;
    }

    public void setGuideAddress(String guideAddress) {
        this.guideAddress = guideAddress;
    }
}
