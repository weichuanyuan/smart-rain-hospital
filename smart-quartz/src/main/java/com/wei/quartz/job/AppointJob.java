package com.wei.quartz.job;

import com.wei.service.ISchedulingDetailService;
import com.wei.utils.spring.SpringApplicationContextUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 不能注入到容器的原因是因为quartz自己实例化了Job对象,所以@Autowired一直是null
 */
public class AppointJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(AppointJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //
        ISchedulingDetailService bean=  SpringApplicationContextUtils.getBean(ISchedulingDetailService.class);
        bean.updateBreakAppoint();
    }
}
