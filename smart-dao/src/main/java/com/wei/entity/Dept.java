package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Dept implements Serializable {

    /**
    * 科室ID
    */
    private Integer deptId;

    /**
    * 科室编码
    */
    private String deptCode;

    /**
    * 科室名称
    */
    private String deptName;

    /**
    * 拼音
    */
    private String deptPinyin;

    /**
    * 科室电话
    */
    private String deptPhone;

    /**
    * 上级ID
    */
    private String deptParentCode;

    /**
    * 排序
    */
    private Integer deptSort;

    /**
    * 科室介绍
    */
    private String deptIntroduce;

    /**
    * 科室地址
    */
    private String deptAddress;

    /**
    * 科室分类编码
    */
    private String deptTypeCode;

    /**
    * 科室分类名称
    */
    private String deptTypeName;

    /**
    * 门诊0 住院1 急诊2
    */
    private String deptFeatures;

    /**
    * 使用状态Y 再用 N停用
    */
    private String deptState;


    public Integer getDeptId() {
        return deptId;
    }
    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getDeptCode() {
        return deptCode;
    }
    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptPinyin() {
        return deptPinyin;
    }
    public void setDeptPinyin(String deptPinyin) {
        this.deptPinyin = deptPinyin;
    }

    public String getDeptPhone() {
        return deptPhone;
    }
    public void setDeptPhone(String deptPhone) {
        this.deptPhone = deptPhone;
    }

    public String getDeptParentCode() {
        return deptParentCode;
    }
    public void setDeptParentCode(String deptParentCode) {
        this.deptParentCode = deptParentCode;
    }

    public Integer getDeptSort() {
        return deptSort;
    }
    public void setDeptSort(Integer deptSort) {
        this.deptSort = deptSort;
    }

    public String getDeptIntroduce() {
        return deptIntroduce;
    }
    public void setDeptIntroduce(String deptIntroduce) {
        this.deptIntroduce = deptIntroduce;
    }

    public String getDeptAddress() {
        return deptAddress;
    }
    public void setDeptAddress(String deptAddress) {
        this.deptAddress = deptAddress;
    }

    public String getDeptTypeCode() {
        return deptTypeCode;
    }
    public void setDeptTypeCode(String deptTypeCode) {
        this.deptTypeCode = deptTypeCode;
    }

    public String getDeptTypeName() {
        return deptTypeName;
    }
    public void setDeptTypeName(String deptTypeName) {
        this.deptTypeName = deptTypeName;
    }

    public String getDeptFeatures() {
        return deptFeatures;
    }
    public void setDeptFeatures(String deptFeatures) {
        this.deptFeatures = deptFeatures;
    }

    public String getDeptState() {
        return deptState;
    }
    public void setDeptState(String deptState) {
        this.deptState = deptState;
    }


    @Override
    public String toString() {
        return "Dept{" +
        "deptId='" + deptId + '\'' +
        "deptCode='" + deptCode + '\'' +
        "deptName='" + deptName + '\'' +
        "deptPinyin='" + deptPinyin + '\'' +
        "deptPhone='" + deptPhone + '\'' +
        "deptParentCode='" + deptParentCode + '\'' +
        "deptSort='" + deptSort + '\'' +
        "deptIntroduce='" + deptIntroduce + '\'' +
        "deptAddress='" + deptAddress + '\'' +
        "deptTypeCode='" + deptTypeCode + '\'' +
        "deptTypeName='" + deptTypeName + '\'' +
        "deptFeatures='" + deptFeatures + '\'' +
        "deptState='" + deptState + '\'' +
    '}';
    }

}
