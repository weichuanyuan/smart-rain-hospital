package com.wei.generator;

import com.wei.common.DbColGenerater;
import com.wei.utils.ApplicationConstants;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.math.BigDecimal;
import java.util.*;


public class Startup {


    public static void main(String[] args) {
        ApplicationContext app = new ClassPathXmlApplicationContext("classpath:generatorTemplateContext.xml");
        Map<String,Object> map = getTableMapper(app);
        // entity代码生成
        Generator generator =  (Generator)app.getBean("entityGenerator");
        generator.GeneratorCode(map);

//        Generator mapperGenerator =  (Generator)app.getBean("mapperGenerator");
//        mapperGenerator.GeneratorCode(map);
//
//        Generator mapperXmlGenerator =  (Generator)app.getBean("mapperXmlGenerator");
//        mapperXmlGenerator.GeneratorCode(map);

//        Generator serviceGenerator =  (Generator)app.getBean("serviceGenerator");
//        serviceGenerator.GeneratorCode(map);
    }

    private static Map<String,JdbcTypeMapper> typesMap = new HashMap<String,JdbcTypeMapper>(){
        {
            put("int",new JdbcTypeMapper(Integer.class,"INTEGER"));
            put("char",new JdbcTypeMapper(String.class,"CHAR"));
            put("varchar",new JdbcTypeMapper(String.class,"VARCHAR"));
            put("datetime", new JdbcTypeMapper(Date.class,"TIMESTAMP"));
            put("decimal", new JdbcTypeMapper(BigDecimal.class,"DECIMAL"));
            put("double", new JdbcTypeMapper(Double.class,"DOUBLE"));
            put("float", new JdbcTypeMapper(Float.class,"FLOAT"));
            put("date", new JdbcTypeMapper(Date.class,"DATE"));
            put("clob", new JdbcTypeMapper(String.class,"CLOB"));
            put("blob", new JdbcTypeMapper(String.class,"BLOB"));
            put("number", new JdbcTypeMapper(Float.class,"FLOAT"));
            put("bigint", new JdbcTypeMapper(Long.class,"BIGINT"));
        }
    };

    public static Map<String,Object> getTableMapper(ApplicationContext app){

        String mapperTableName = "tb_dic_charge_fee";// 表名称
        String mapperEntityPackageName = "com.wei.entity";// 实体对象包名
        String mapperEntityPath = "src/main/java";// 生成路径
        String mapperReadDaoPackageName = "com.wei.dao.read";// 读dao包名
        String mapperWriteDaoPackageName = "com.wei.dao.write"; //写dao包名
        String mapperReadXmlPath = "src/main/resources/mapper/read";
        String mapperReadExtendXmlPath = "src/main/resources/mapper/read/extend";
        String mapperWriteXmlPath = "src/main/resources/mapper/write";
        String mapperWriteExtendXmlPath = "src/main/resources/mapper/write/extend";
        String mapperServicePackageName = "com.wei.service";// service接口层包名
        String mapperServicePath = "src/main/java";// service接口层生成路径
        String mapperServiceImplPackageName = "com.wei.service.impl";// service实现层包名
        String mapperServiceImplPath = "src/main/java";// service实现层生成路径

        JdbcTemplate jdbcTemplate= app.getBean(JdbcTemplate.class);

        // 主键-->PRI  | 唯一索引 -->UNI  一般索引 -->MUL
        String sql = "select column_name as columnName,column_comment as columnComment,data_type as dataType,column_key as columnKey\n" +
                "from information_schema.columns \n" +
                "where table_name='" + mapperTableName + "' ";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);

        Map<String,Object> result = new HashMap<>();

        String entityName = DbColGenerater.generaterClassTableName(mapperTableName);

        result.put("tableName",mapperTableName);
        result.put("entityName",entityName);
        result.put("packageName",mapperEntityPackageName);
        result.put("entityPath",mapperEntityPath);
        result.put("readPackageName",mapperReadDaoPackageName);
        result.put("readMapperName",entityName+"ReadMapper");
        result.put("writePackageName",mapperWriteDaoPackageName);
        result.put("writeMapperName",entityName+"OptMapper");
        result.put("readXmlPath",mapperReadXmlPath);
        result.put("readExtendXmlPath",mapperReadExtendXmlPath);
        result.put("writeXmlPath",mapperWriteXmlPath);
        result.put("writeExtendXmlPath",mapperWriteExtendXmlPath);
        result.put("readExtendMapperName",entityName+"ReadExtendMapper");
        result.put("writeExtendMapperName",entityName+"OptExtendMapper");
        result.put("servicePackageName",mapperServicePackageName);
        result.put("servicePath",mapperServicePath);
        result.put("serviceName","I"+entityName+"Service");
        result.put("serviceImplPackageName",mapperServiceImplPackageName);
        result.put("serviceImplPath",mapperServiceImplPath);
        result.put("serviceImplName",entityName+"ServiceImpl");

        List<Map<String,String>> cols = new ArrayList<>();
        for (Map<String,Object> map : maps){
            Map<String,String> param = new HashMap<>();

            String columnName = map.get("columnName").toString();
            String columnComment = map.get("columnComment").toString();
            String dataType = map.get("dataType").toString();
            String columnKey = map.get("columnKey").toString();

            if(ApplicationConstants.PRI.equals(columnKey)){
                result.put("id",columnName);
                result.put("fieldId",DbColGenerater.generaterClassFieldName(columnName));
                result.put("idJdbcType",typesMap.get(dataType).getJdbcType());
                result.put("idJavaType",typesMap.get(dataType).getJavaType().getSimpleName());
            }

            param.put("remark",columnComment);
            param.put("fieldName",DbColGenerater.generaterClassFieldName(columnName));
            param.put("type",typesMap.get(dataType).getJavaType().getSimpleName());
            param.put("jdbcType",typesMap.get(dataType).getJdbcType());
            param.put("columnName",columnName);

            cols.add(param);
        }
        result.put("fields",cols);

        return result;
    }
}
