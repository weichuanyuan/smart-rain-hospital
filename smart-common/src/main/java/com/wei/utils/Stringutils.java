package com.wei.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Iterator;

public final class Stringutils {
    private Stringutils() {
    }


    /**
     * 拼接字符串第二个字符串第一个字母大写
     */
    public static String concatCapitalize(String concatStr, final String str) {
        if (isEmptyOrWhiteSpace(concatStr)) {
            concatStr = ApplicationConstants.EMPTY;
        }
        if (str == null || str.length() == 0) {
            return str;
        }

        final char firstChar = str.charAt(0);
        if (Character.isTitleCase(firstChar)) {
            // already capitalized
            return str;
        }

        return concatStr + Character.toTitleCase(firstChar) + str.substring(1);
    }
    /**
     * 猜测方法属性对应的 Getter 名称，具体规则请参考 JavaBeans 规范
     *
     * @param name 属性名称
     * @param type 属性类型
     * @return 返回猜测的名称
     * @deprecated 3.3.2
     */
    @Deprecated
    public static String guessGetterName(String name, Class<?> type) {
        return boolean.class == type ? name.startsWith("is") ? name : "is" + upperFirst(name) : "get" + upperFirst(name);
    }
    /**
     * 大写第一个字母
     *
     * @param src 源字符串
     * @return 返回第一个大写后的字符串
     */
    public static String upperFirst(String src) {
        if (Character.isLowerCase(src.charAt(0))) {
            return 1 == src.length() ? src.toUpperCase() : Character.toUpperCase(src.charAt(0)) + src.substring(1);
        }
        return src;
    }
    /**
     * 是否是空
     *
     * @param source 源字符串
     * @return
     */
    public static boolean isEmptyOrWhiteSpace(CharSequence source) {
        return org.apache.commons.lang3.StringUtils.isBlank(source);
    }

    /**
     * 是否是空
     *
     * @param source 源字符串
     * @return
     */
    public static boolean isEmpty(CharSequence source) {
        return org.apache.commons.lang3.StringUtils.isEmpty(source);
    }

    /**
     * 是否全为数字
     *
     * @param source 源字符串
     * @return
     */
    public static boolean isNumeric(CharSequence source) {
        return org.apache.commons.lang3.StringUtils.isNumeric(source);
    }

    /**
     * 是否全部为大写字符
     *
     * @param source
     * @return
     */
    public static boolean isAllUpperCase(CharSequence source) {
        return org.apache.commons.lang3.StringUtils.isAllUpperCase(source);
    }

    /**
     * 是否以prefix开始
     * @param source
     * @param prefix
     * @return
     */
    public static boolean startWith(CharSequence source,CharSequence prefix){
        return org.apache.commons.lang3.StringUtils.startsWith(source,prefix);
    }

    /**
     * 是否以prefix开始 忽略大小写
     * @param source
     * @param prefix
     * @return
     */
    public static boolean startsWithIgnoreCase(CharSequence source,CharSequence prefix){
        return org.apache.commons.lang3.StringUtils.startsWithIgnoreCase(source,prefix);
    }

    /**
     * 是否是大写字符
     * @param source
     * @return
     */
    public static boolean isUpperCaseChar(char source){
        return Character.isUpperCase(source);
    }

    /**
     * 是否是小写字符
     * @param source
     * @return
     */
    public static  boolean isLowerCaseChar(char source){
        return Character.isLowerCase(source);
    }



    /**
     * 转化成大写
     * @param source
     * @return
     */
    public static char toUpper(char source){
        return Character.toUpperCase(source);
    }

    /**
     * 转换成小写
     * @param source
     * @return
     */
    public static char toLower(char source){
        return Character.toLowerCase(source);
    }

    /**
     * 将字符转换成String
     * @param source
     * @return
     */
    public static String toString(char source){
        return Character.toString(source);
    }

    /**
     * 截取从start开始到结束
     * @param source
     * @param start
     * @return
     */
    public static String subStr(String source,int start){
        return org.apache.commons.lang3.StringUtils.substring(source,start);
    }

    /**
     * 截取从start开始 到 end结束
     * @param soutce
     * @param start
     * @param end
     * @return
     */
    public static String subStr(String soutce,int start,int end){
        return org.apache.commons.lang3.StringUtils.substring(soutce,start,end);
    }

    /**
     * search 在 source中位置索引
     * @param source
     * @param search
     * @return
     */
    public static int indexOf(CharSequence source,CharSequence search){
        return org.apache.commons.lang3.StringUtils.indexOf(source,search);
    }

    /**
     * 将传入数组中的各个元素连接成一个字符串，空的对象或空字符串将被处理成空字符串。
     *
     * @param <T> 需要连接成字符串的具体类型
     * @param elements 需要被链接成字符串的值, 允许为null
     * @return 连接后的字符串, 如果传入的数组为null则返回""
     */
    public static <T> String join(T... elements) {
        return org.apache.commons.lang3.StringUtils.join(elements);
    }

    /**
     * 将对象数组中的各个元素用指定的字符连接成一个字符串，在数组中空对象或空字符串将被当作空字符串处理。
     *
     * @param array 需要被处理的数组, 允许为 null
     * @param separator 用于连接的字符
     * @return 链接好的字符串, 数组为null时返回null
     */
    public static String join(Object[] array, char separator) {
        return org.apache.commons.lang3.StringUtils.join(array, separator);
    }

    /**
     * 用指定的分隔字符将数组中的内容链接成一个字符串，从startIndex下标开始到endIndex结束。
     *
     * @param array 需要被处理的数组, 允许为 null
     * @param separator 用于连接的字符
     * @param startIndex 开始连接的下标值
     * @param endIndex 结束连接的下标值
     * @return 链接好的字符串, 数组为空时返回null
     */
    public static String join(Object[] array, char separator, int startIndex, int endIndex) {
        return org.apache.commons.lang3.StringUtils.join(array, separator, startIndex, endIndex);
    }

    /**
     * 用指定的分隔字符串将数组中的内容链接成一个字符串。
     *
     * @param array 需要被处理的数组, 允许为 null
     * @param separator 连接用的字符串, null 被当作 ""处理
     * @return 链接好的字符串, 数组为null时返回null
     */
    public static String join(Object[] array, String separator) {
        return org.apache.commons.lang3.StringUtils.join(array, separator);
    }

    /**
     * 用指定的分隔字符串将数组中的内容链接成一个字符串，从startIndex下标开始到endIndex结束。
     *
     * @param array 需要被处理的数组, 允许为 null
     * @param separator 连接用的字符串, null 被当作 ""处理
     * @param startIndex 开始连接的下标值
     * @param endIndex 结束连接的下标值
     * @return 链接好的字符串, 数组为[nul]或[]时返回"",为null时返回null
     */
    public static String join(Object[] array, String separator, int startIndex, int endIndex) {
        return org.apache.commons.lang3.StringUtils.join(array, separator, startIndex, endIndex);
    }

    /**
     * 用指定字符将迭代器中的各个元素连接成一个字符串。
     *
     * @param iterator 需要被处理的iterator, 允许为null
     * @param separator 连接用的字符串
     * @return 链接好的字符串, iterator为null时返回null
     */
    public static String join(Iterator<?> iterator, char separator) {
        return org.apache.commons.lang3.StringUtils.join(iterator, separator);
    }

    /**
     * 用指定字符串将迭代器中的各个元素连接成一个字符串。
     *
     * @param iterator 需要被处理的iterator, 允许为null
     * @param separator 连接用的字符串，null 被当作 ""处理
     * @return 链接好的字符串, iterator为null时返回null
     */
    public static String join(Iterator<?> iterator, String separator) {
        return org.apache.commons.lang3.StringUtils.join(iterator, separator);
    }

    /**
     * 用指定的字符将Iterable中的元素链接成一个字符串；
     *
     * @param iterable 需要被处理的iterable, 允许为null
     * @param separator 连接用的字符串
     * @return 链接好的字符串, iterable为null时返回null
     */
    public static String join(Iterable<?> iterable, char separator) {
        return org.apache.commons.lang3.StringUtils.join(iterable, separator);
    }

    /**
     * 用指定的字符串将Iterable中的元素链接成一个字符串。
     *
     * @param iterable 需要被处理的iterable, 允许为null
     * @param separator 连接用的字符串，null 被当作 ""处理
     * @return 链接好的字符串, iterable为null时返回null
     */
    public static String join(Iterable<?> iterable, String separator) {
        return org.apache.commons.lang3.StringUtils.join(iterable, separator);
    }

    /**
     * 安全的进行字符串 format
     *
     * @param target 目标字符串
     * @param params format 参数
     * @return format 后的
     */
    public static String format(String target, Object... params) {
        return String.format(target, params);
    }

    public static  String[] split(String source,String separatorChars){
        return org.apache.commons.lang3.StringUtils.split(source,separatorChars);
    }

}
