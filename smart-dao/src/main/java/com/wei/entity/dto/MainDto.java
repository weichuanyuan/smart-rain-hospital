package com.wei.entity.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class MainDto {
    /**
     * 处方
     */
    private List<PrescriptionDto> prescriptionDtoList;
    /**
     * 主键
     */
    private Integer mainId;
    /**
     * 开单医生
     */
    private String prescriptionDrname;

    /**
     * 处方总金额
     */
    private BigDecimal chargeFee;
    /**
     * 开单科室
     */
    private String prescriptionDeptName;
    /**
     * 处方时间
     */
    private Date prescriptionTime;

    public List<PrescriptionDto> getPrescriptionDtoList() {
        return prescriptionDtoList;
    }

    public void setPrescriptionDtoList(List<PrescriptionDto> prescriptionDtoList) {
        this.prescriptionDtoList = prescriptionDtoList;
    }

    public Integer getMainId() {
        return mainId;
    }

    public void setMainId(Integer mainId) {
        this.mainId = mainId;
    }

    public String getPrescriptionDrname() {
        return prescriptionDrname;
    }

    public void setPrescriptionDrname(String prescriptionDrname) {
        this.prescriptionDrname = prescriptionDrname;
    }

    public BigDecimal getChargeFee() {
        return chargeFee;
    }

    public void setChargeFee(BigDecimal chargeFee) {
        this.chargeFee = chargeFee;
    }

    public String getPrescriptionDeptName() {
        return prescriptionDeptName;
    }

    public void setPrescriptionDeptName(String prescriptionDeptName) {
        this.prescriptionDeptName = prescriptionDeptName;
    }

    public Date getPrescriptionTime() {
        return prescriptionTime;
    }

    public void setPrescriptionTime(Date prescriptionTime) {
        this.prescriptionTime = prescriptionTime;
    }
}
