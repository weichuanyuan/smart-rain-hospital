package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DicChargeFee implements Serializable {

    /**
    * 费用编码
    */
    private String feeCode;

    /**
    * 费用名称
    */
    private String feeName;

    /**
    * 费用类型编码
    */
    private String feeTypeCode;

    /**
    * 费用类型名称
    */
    private String feeTypeName;

    /**
    * 费用分类id
    */
    private String feeClassCode;

    /**
    * 费用分类名称
    */
    private String feeClassName;

    /**
    * 规格
    */
    private String feeSpecification;

    /**
    * 单位编码
    */
    private String feeUnitCode;

    /**
    * 单位名称
    */
    private String feeUnitName;

    /**
    * 备注
    */
    private String feeRemark;

    /**
    * 费用使用范围 门诊0 住院1 急诊2  体检 3
    */
    private String feeLimit;

    /**
    * 单价
    */
    private BigDecimal feePrice;

    /**
    * 操作时间
    */
    private Date feeCreateDate;

    /**
    * 操作人
    */
    private String feeCreateCode;

    /**
    * 使用标志
    */
    private String feeState;

    /**
    * 病案首页费用分类编码
    */
    private String feeMedicalRecodeCode;

    /**
    * 病案首页费用分类名称
    */
    private String feeMedicalRecodeName;

    /**
    * 小孩费用单价
    */
    private BigDecimal feeChildPrice;


    public String getFeeCode() {
        return feeCode;
    }
    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }

    public String getFeeName() {
        return feeName;
    }
    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public String getFeeTypeCode() {
        return feeTypeCode;
    }
    public void setFeeTypeCode(String feeTypeCode) {
        this.feeTypeCode = feeTypeCode;
    }

    public String getFeeTypeName() {
        return feeTypeName;
    }
    public void setFeeTypeName(String feeTypeName) {
        this.feeTypeName = feeTypeName;
    }

    public String getFeeClassCode() {
        return feeClassCode;
    }
    public void setFeeClassCode(String feeClassCode) {
        this.feeClassCode = feeClassCode;
    }

    public String getFeeClassName() {
        return feeClassName;
    }
    public void setFeeClassName(String feeClassName) {
        this.feeClassName = feeClassName;
    }

    public String getFeeSpecification() {
        return feeSpecification;
    }
    public void setFeeSpecification(String feeSpecification) {
        this.feeSpecification = feeSpecification;
    }

    public String getFeeUnitCode() {
        return feeUnitCode;
    }
    public void setFeeUnitCode(String feeUnitCode) {
        this.feeUnitCode = feeUnitCode;
    }

    public String getFeeUnitName() {
        return feeUnitName;
    }
    public void setFeeUnitName(String feeUnitName) {
        this.feeUnitName = feeUnitName;
    }

    public String getFeeRemark() {
        return feeRemark;
    }
    public void setFeeRemark(String feeRemark) {
        this.feeRemark = feeRemark;
    }

    public String getFeeLimit() {
        return feeLimit;
    }
    public void setFeeLimit(String feeLimit) {
        this.feeLimit = feeLimit;
    }

    public BigDecimal getFeePrice() {
        return feePrice;
    }
    public void setFeePrice(BigDecimal feePrice) {
        this.feePrice = feePrice;
    }

    public Date getFeeCreateDate() {
        return feeCreateDate;
    }
    public void setFeeCreateDate(Date feeCreateDate) {
        this.feeCreateDate = feeCreateDate;
    }

    public String getFeeCreateCode() {
        return feeCreateCode;
    }
    public void setFeeCreateCode(String feeCreateCode) {
        this.feeCreateCode = feeCreateCode;
    }

    public String getFeeState() {
        return feeState;
    }
    public void setFeeState(String feeState) {
        this.feeState = feeState;
    }

    public String getFeeMedicalRecodeCode() {
        return feeMedicalRecodeCode;
    }
    public void setFeeMedicalRecodeCode(String feeMedicalRecodeCode) {
        this.feeMedicalRecodeCode = feeMedicalRecodeCode;
    }

    public String getFeeMedicalRecodeName() {
        return feeMedicalRecodeName;
    }
    public void setFeeMedicalRecodeName(String feeMedicalRecodeName) {
        this.feeMedicalRecodeName = feeMedicalRecodeName;
    }

    public BigDecimal getFeeChildPrice() {
        return feeChildPrice;
    }
    public void setFeeChildPrice(BigDecimal feeChildPrice) {
        this.feeChildPrice = feeChildPrice;
    }


    @Override
    public String toString() {
        return "DicChargeFee{" +
        "feeCode='" + feeCode + '\'' +
        "feeName='" + feeName + '\'' +
        "feeTypeCode='" + feeTypeCode + '\'' +
        "feeTypeName='" + feeTypeName + '\'' +
        "feeClassCode='" + feeClassCode + '\'' +
        "feeClassName='" + feeClassName + '\'' +
        "feeSpecification='" + feeSpecification + '\'' +
        "feeUnitCode='" + feeUnitCode + '\'' +
        "feeUnitName='" + feeUnitName + '\'' +
        "feeRemark='" + feeRemark + '\'' +
        "feeLimit='" + feeLimit + '\'' +
        "feePrice='" + feePrice + '\'' +
        "feeCreateDate='" + feeCreateDate + '\'' +
        "feeCreateCode='" + feeCreateCode + '\'' +
        "feeState='" + feeState + '\'' +
        "feeMedicalRecodeCode='" + feeMedicalRecodeCode + '\'' +
        "feeMedicalRecodeName='" + feeMedicalRecodeName + '\'' +
        "feeChildPrice='" + feeChildPrice + '\'' +
    '}';
    }

}
