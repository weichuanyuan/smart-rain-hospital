package com.wei.controller;

import com.wei.annotation.RepeatVerify;
import com.wei.dao.Page;
import com.wei.entity.SysConfig;
import com.wei.service.ISysConfigService;
import com.wei.web.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/sysConfig")
@Api(value = "/SysConfigController", tags = "系统配置")
public class SysConfigController {

    @Autowired
    private ISysConfigService sysConfigService;
    /**
     * 根据条件获取List<SysConfig>
     * @param page
     * @return
     */
    @ResponseBody
    @PostMapping("/getAll")
    @ApiOperation("根据条件获取List<SysConfig>")
    public ResultVo getAll(@RequestBody Page<SysConfig> page){
        return new ResultVo("200","查询成功!",sysConfigService.getAll(page));
    }

}
