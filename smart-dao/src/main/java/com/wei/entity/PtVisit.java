package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PtVisit implements Serializable {

    /**
    * 主键
    */
    private Integer visitId;

    /**
    * 就诊次数
    */
    private Integer visitSeries;

    /**
    * 病人id
    */
    private Integer ptId;

    /**
    * 门诊0 住院1 急诊2 体检 3
    */
    private String visitType;

    /**
    * 病人姓名
    */
    private String ptName;

    /**
    * 挂号科室编码
    */
    private String visitDeptCode;

    /**
    * 挂号科室名称
    */
    private String visitDeptName;

    /**
    * 挂号时间
    */
    private Date visitTime;

    /**
    * 状态1 已挂号 2 已退号
    */
    private String visitStatus;

    /**
    * 挂号医生
    */
    private String drCode;

    /**
    * 操作人
    */
    private String optCode;

    /**
    * 退号时间
    */
    private Date backTime;

    /**
    * 总挂号金额
    */
    private BigDecimal registerFee;

    /**
    * 0 未就诊 1 已就诊
    */
    private String medicalStatus;

    /**
    * 就诊时间
    */
    private Date medicalTime;

    /**
    * 医保类型
    */
    private String miType;

    /**
    * 慢病类型
    */
    private String chronicType;

    /**
    * 特殊病类型
    */
    private String specialType;

    /**
    * 绿色通道
    */
    private String greenChannel;

    /**
    * icd10诊断名称
    */
    private String diagnoseName;

    /**
    * icd10诊断编码
    */
    private String diagnoseCode;


    public Integer getVisitId() {
        return visitId;
    }
    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public Integer getVisitSeries() {
        return visitSeries;
    }
    public void setVisitSeries(Integer visitSeries) {
        this.visitSeries = visitSeries;
    }

    public Integer getPtId() {
        return ptId;
    }
    public void setPtId(Integer ptId) {
        this.ptId = ptId;
    }

    public String getVisitType() {
        return visitType;
    }
    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public String getPtName() {
        return ptName;
    }
    public void setPtName(String ptName) {
        this.ptName = ptName;
    }

    public String getVisitDeptCode() {
        return visitDeptCode;
    }
    public void setVisitDeptCode(String visitDeptCode) {
        this.visitDeptCode = visitDeptCode;
    }

    public String getVisitDeptName() {
        return visitDeptName;
    }
    public void setVisitDeptName(String visitDeptName) {
        this.visitDeptName = visitDeptName;
    }

    public Date getVisitTime() {
        return visitTime;
    }
    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

    public String getVisitStatus() {
        return visitStatus;
    }
    public void setVisitStatus(String visitStatus) {
        this.visitStatus = visitStatus;
    }

    public String getDrCode() {
        return drCode;
    }
    public void setDrCode(String drCode) {
        this.drCode = drCode;
    }

    public String getOptCode() {
        return optCode;
    }
    public void setOptCode(String optCode) {
        this.optCode = optCode;
    }

    public Date getBackTime() {
        return backTime;
    }
    public void setBackTime(Date backTime) {
        this.backTime = backTime;
    }

    public BigDecimal getRegisterFee() {
        return registerFee;
    }
    public void setRegisterFee(BigDecimal registerFee) {
        this.registerFee = registerFee;
    }

    public String getMedicalStatus() {
        return medicalStatus;
    }
    public void setMedicalStatus(String medicalStatus) {
        this.medicalStatus = medicalStatus;
    }

    public Date getMedicalTime() {
        return medicalTime;
    }
    public void setMedicalTime(Date medicalTime) {
        this.medicalTime = medicalTime;
    }

    public String getMiType() {
        return miType;
    }
    public void setMiType(String miType) {
        this.miType = miType;
    }

    public String getChronicType() {
        return chronicType;
    }
    public void setChronicType(String chronicType) {
        this.chronicType = chronicType;
    }

    public String getSpecialType() {
        return specialType;
    }
    public void setSpecialType(String specialType) {
        this.specialType = specialType;
    }

    public String getGreenChannel() {
        return greenChannel;
    }
    public void setGreenChannel(String greenChannel) {
        this.greenChannel = greenChannel;
    }

    public String getDiagnoseName() {
        return diagnoseName;
    }
    public void setDiagnoseName(String diagnoseName) {
        this.diagnoseName = diagnoseName;
    }

    public String getDiagnoseCode() {
        return diagnoseCode;
    }
    public void setDiagnoseCode(String diagnoseCode) {
        this.diagnoseCode = diagnoseCode;
    }


    @Override
    public String toString() {
        return "PtVisit{" +
        "visitId='" + visitId + '\'' +
        "visitSeries='" + visitSeries + '\'' +
        "ptId='" + ptId + '\'' +
        "visitType='" + visitType + '\'' +
        "ptName='" + ptName + '\'' +
        "visitDeptCode='" + visitDeptCode + '\'' +
        "visitDeptName='" + visitDeptName + '\'' +
        "visitTime='" + visitTime + '\'' +
        "visitStatus='" + visitStatus + '\'' +
        "drCode='" + drCode + '\'' +
        "optCode='" + optCode + '\'' +
        "backTime='" + backTime + '\'' +
        "registerFee='" + registerFee + '\'' +
        "medicalStatus='" + medicalStatus + '\'' +
        "medicalTime='" + medicalTime + '\'' +
        "miType='" + miType + '\'' +
        "chronicType='" + chronicType + '\'' +
        "specialType='" + specialType + '\'' +
        "greenChannel='" + greenChannel + '\'' +
        "diagnoseName='" + diagnoseName + '\'' +
        "diagnoseCode='" + diagnoseCode + '\'' +
    '}';
    }

}
