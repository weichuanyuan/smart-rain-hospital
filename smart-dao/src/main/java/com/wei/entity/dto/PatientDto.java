package com.wei.entity.dto;

public class PatientDto {
    /**
     * 门诊号住院号
     */
    private String ioNo;
    /**
     * 姓名
     */
    private String name;
    /**
     * 卡号
     */
    private String cardno;
    /**
     * 卡号类型
     */
    private String cardtype;
    /**
     * 电话号码
     */
    private String phone;
    /**
     * 性别
     */
    private String sex;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 县
     */
    private String county;
    /**
     * 详细地址
     */
    private String addrDetail;
    /**
     * 小孩标志 Y 标识小孩 N 标识成人
     */
    private String child;
    /**
     * 监护人 卡号类型
     */
    private String guardianCardtype;
    /**
     * 监护人 卡号
     */
    private String guardianCardno;
    /**
     * 验证码
     */
    private String checkNum;

    public String getCheckNum() {
        return checkNum;
    }

    public void setCheckNum(String checkNum) {
        this.checkNum = checkNum;
    }

    /**
     * 当前患者
     */
    private String crtFlag;

    public String getCrtFlag() {
        return crtFlag;
    }

    public void setCrtFlag(String crtFlag) {
        this.crtFlag = crtFlag;
    }


    public String getIoNo() {
        return ioNo;
    }

    public void setIoNo(String ioNo) {
        this.ioNo = ioNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddrDetail() {
        return addrDetail;
    }

    public void setAddrDetail(String addrDetail) {
        this.addrDetail = addrDetail;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getGuardianCardtype() {
        return guardianCardtype;
    }

    public void setGuardianCardtype(String guardianCardtype) {
        this.guardianCardtype = guardianCardtype;
    }

    public String getGuardianCardno() {
        return guardianCardno;
    }

    public void setGuardianCardno(String guardianCardno) {
        this.guardianCardno = guardianCardno;
    }
}
