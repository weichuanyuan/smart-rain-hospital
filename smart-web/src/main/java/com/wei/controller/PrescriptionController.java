package com.wei.controller;

import com.wei.annotation.AuthorityVerify;
import com.wei.annotation.RepeatVerify;
import com.wei.service.IPrescriptionService;
import com.wei.web.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/prescription")
@Api(value = "/prescriptionController", tags = "处方费用相关api")
@AuthorityVerify
public class PrescriptionController {
    @Autowired
    private IPrescriptionService prescriptionService;

    @ResponseBody
    @PostMapping("/getMainDtoList")
    @ApiOperation("根据条件查询处方汇总信息")
    public ResultVo getMainDtoList(@RequestBody Map<String,String> map) {
        return new ResultVo("200","查询成功",prescriptionService.getPrescriptionDtoList(map));
    }
    @ResponseBody
    @PostMapping("/charge")
    @ApiOperation("缴费")
    @RepeatVerify(primary = {"prescriptionIds"})
    public ResultVo charge(@RequestBody Map<String,String> map){
        return new ResultVo("200","查询成功",prescriptionService.charge(map));
    }
}
