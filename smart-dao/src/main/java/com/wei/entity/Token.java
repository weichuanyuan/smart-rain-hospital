package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Token implements Serializable {

    /**
    * 系统token
    */
    private String token;

    /**
    * 患者/员工id
    */
    private Integer id;

    /**
    * 01 员工 02 患者
    */
    private String type;

    /**
    * 操作时间
    */
    private Date modDt;


    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Date getModDt() {
        return modDt;
    }
    public void setModDt(Date modDt) {
        this.modDt = modDt;
    }


    @Override
    public String toString() {
        return "Token{" +
        "token='" + token + '\'' +
        "id='" + id + '\'' +
        "type='" + type + '\'' +
        "modDt='" + modDt + '\'' +
    '}';
    }

}
