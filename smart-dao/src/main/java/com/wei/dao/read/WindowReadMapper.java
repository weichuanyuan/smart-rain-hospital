package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.Window;

import java.util.List;

public interface WindowReadMapper extends IBaseReadDao<Window,Integer> {
    List<Window> getWindowListByDeptCodeList(List<String> deptCodeList);
}