package com.wei.dao;

public enum Direction {
    ASC("asc"),
    DESC("desc");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    Direction(String value){
          this.value = value;
      }
}
