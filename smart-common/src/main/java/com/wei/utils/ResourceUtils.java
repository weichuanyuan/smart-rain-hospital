package com.wei.utils;

import java.io.File;
import java.io.IOException;

public class ResourceUtils {
    private ResourceUtils(){}

    public static String getClasspath(){
       return ResourceUtils.class.getResource("/").getPath();
    }

    public static String getProjectRootPath() throws IOException {
        File directory = new File("");
        return directory.getCanonicalPath();
    }
}
