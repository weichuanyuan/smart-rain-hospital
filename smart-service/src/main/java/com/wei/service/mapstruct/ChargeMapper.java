package com.wei.service.mapstruct;

import com.wei.entity.Charge;
import com.wei.entity.dto.ChargeDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ChargeMapper {
    ChargeMapper INSTANCE = Mappers.getMapper(ChargeMapper.class);
    @Mappings({
            @Mapping(source = "feeSpecification", target="specifications"),
            @Mapping(source = "feeSum", target="chargeFee"),
            @Mapping(source = "fee", target="chargePrice")
    })
    ChargeDto ChargeToChargeDto(Charge charge);
    List<ChargeDto> ChargeListToChargeDtoList(List<Charge> charge);

}
