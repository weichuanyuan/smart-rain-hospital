package com.wei.entity.vo;

import com.wei.entity.Appointment;

import java.math.BigDecimal;

public class AppointmentVo extends Appointment {
    private String deptAddress;
    private String drName;
    private String levelName;
    private String deptName;
    private String datetime;
    private String state;
    private String regNo;
    private BigDecimal feePrice;
    private BigDecimal childfeePrice;

    public String getDrName() {
        return drName;
    }

    public void setDrName(String drName) {
        this.drName = drName;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public BigDecimal getFeePrice() {
        return feePrice;
    }

    public void setFeePrice(BigDecimal feePrice) {
        this.feePrice = feePrice;
    }

    public BigDecimal getChildfeePrice() {
        return childfeePrice;
    }

    public void setChildfeePrice(BigDecimal childfeePrice) {
        this.childfeePrice = childfeePrice;
    }
    public String getDeptAddress() {
        return deptAddress;
    }

    public void setDeptAddress(String deptAddress) {
        this.deptAddress = deptAddress;
    }
}
