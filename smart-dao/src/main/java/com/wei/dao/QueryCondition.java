package com.wei.dao;

public enum QueryCondition {
    /**
     * equal
     */
    EQ("="),
    /**
     * not equal
     */
    NQ("!="),
    /**
     * Greater than or equal to
     */
    GE(">="),
    /**
     * Greater
     */
    GT(">"),
    /**
     * less
     */
    Ls("<"),
    /**
     * Less than or equal to
     */
    LE("<="),
    /**
     * like
     */
    LIKE("like"),
    /**
     * not like
     */
    NLIKE("nlike"),
    /**
     * left like
     */
    LLILE("llike"),
    /**
     * right like
     */
    RLIKE("rlike"),
    /**
     * in
     */
    IN("in"),
    /**
     * not in
     */
    NN("nin");

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;
    QueryCondition(String value){
        this.value = value;
    }

}
