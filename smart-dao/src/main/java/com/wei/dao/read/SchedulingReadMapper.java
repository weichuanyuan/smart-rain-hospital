package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.Scheduling;
import com.wei.entity.vo.SchedulingVo;

import java.util.List;
import java.util.Map;

public interface SchedulingReadMapper extends IBaseReadDao<Scheduling,Integer> {

     List<SchedulingVo> getSchedulingByCondition(Map<String,String> maps);

}