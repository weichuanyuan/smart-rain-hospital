package com.wei.entity.dto;

import java.math.BigDecimal;
import java.util.Date;

public class AppointRecordDto {
    private String drName;
    private String levelName;
    private String deptName;
    private String datetime;
    private String state;
    private String regNo;
    private BigDecimal feePrice;
    private BigDecimal childfeePrice;
    private Integer tbAppointId;
    private String appointName;
    private Integer appoinitPtId;
    private String appointIoNo;
    private String appointDrCode;
    private String appointDeptCode;
    private String appointApn;
    private Date appointDate;
    private String appointTime;
    private String appointState;
    private String appointUseName;
    private Integer schedulingDetailId;
    private String deptAddress;

    public String getDeptAddress() {
        return deptAddress;
    }

    public void setDeptAddress(String deptAddress) {
        this.deptAddress = deptAddress;
    }

    public String getDrName() {
        return drName;
    }

    public void setDrName(String drName) {
        this.drName = drName;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public BigDecimal getFeePrice() {
        return feePrice;
    }

    public void setFeePrice(BigDecimal feePrice) {
        this.feePrice = feePrice;
    }

    public BigDecimal getChildfeePrice() {
        return childfeePrice;
    }

    public void setChildfeePrice(BigDecimal childfeePrice) {
        this.childfeePrice = childfeePrice;
    }

    public Integer getTbAppointId() {
        return tbAppointId;
    }

    public void setTbAppointId(Integer tbAppointId) {
        this.tbAppointId = tbAppointId;
    }

    public String getAppointName() {
        return appointName;
    }

    public void setAppointName(String appointName) {
        this.appointName = appointName;
    }

    public Integer getAppoinitPtId() {
        return appoinitPtId;
    }

    public void setAppoinitPtId(Integer appoinitPtId) {
        this.appoinitPtId = appoinitPtId;
    }

    public String getAppointIoNo() {
        return appointIoNo;
    }

    public void setAppointIoNo(String appointIoNo) {
        this.appointIoNo = appointIoNo;
    }

    public String getAppointDrCode() {
        return appointDrCode;
    }

    public void setAppointDrCode(String appointDrCode) {
        this.appointDrCode = appointDrCode;
    }

    public String getAppointDeptCode() {
        return appointDeptCode;
    }

    public void setAppointDeptCode(String appointDeptCode) {
        this.appointDeptCode = appointDeptCode;
    }

    public String getAppointApn() {
        return appointApn;
    }

    public void setAppointApn(String appointApn) {
        this.appointApn = appointApn;
    }

    public Date getAppointDate() {
        return appointDate;
    }

    public void setAppointDate(Date appointDate) {
        this.appointDate = appointDate;
    }

    public String getAppointTime() {
        return appointTime;
    }

    public void setAppointTime(String appointTime) {
        this.appointTime = appointTime;
    }

    public String getAppointState() {
        return appointState;
    }

    public void setAppointState(String appointState) {
        this.appointState = appointState;
    }

    public String getAppointUseName() {
        return appointUseName;
    }

    public void setAppointUseName(String appointUseName) {
        this.appointUseName = appointUseName;
    }

    public Integer getSchedulingDetailId() {
        return schedulingDetailId;
    }

    public void setSchedulingDetailId(Integer schedulingDetailId) {
        this.schedulingDetailId = schedulingDetailId;
    }
}
