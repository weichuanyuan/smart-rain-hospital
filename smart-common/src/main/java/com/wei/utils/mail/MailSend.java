package com.wei.utils.mail;

import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MailSend {

    private static final String HOST = "smtp.qq.com";//邮件服务器的SMTP地址
    private static final String AUTH = "true"; //需要用户认证
    private static final String USER = "329274634";//用户名（注意：如果使用foxmail邮箱，此处user为qq号）
    private static final String PASS = "tsmvwdokmypxbjjd";//用密码（注意，某些邮箱需要为SMTP服务单独设置密码，详情查看相关帮助）
    private static final String FROM = "329274634@qq.com";//发件人邮箱

    /**
     * 发送邮件地址
     *
     * @param str
     * @return
     */
    private static InternetAddress[] Address(String str) {
        //多个接收账号
        InternetAddress[] address = null;
        try {
            List list = new ArrayList();//不能使用string类型的类型，这样只能发送一个收件人
            if (str != null || "".equals(str)) {
                String[] median = str.split(",");//对输入的多个邮件进行逗号分割
                for (int i = 0; i < median.length; i++) {
                    //单个添加
                    list.add(new InternetAddress(median[i]));
                }
            } else {
                //设置默认收件人的邮箱
                list.add(new InternetAddress("默认收件人的邮箱"));
            }
            //集合转数组
            address = (InternetAddress[]) list.toArray(new InternetAddress[list.size()]);


        } catch (AddressException e) {
            e.printStackTrace();
        }
        return address;
    }

    /**
     * 发送邮件2
     *
     * @param mail
     * @param title
     * @param content
     */
    public static void sendQQMail(String mail, String title, String content) {
        //系统属性
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", AUTH);
        properties.setProperty("mail.smtp.host", HOST);    // 发件人的邮箱的 SMTP 服务器地址
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USER, PASS);
            }
        });
        //创建email
        MimeMessage message = new MimeMessage(session);
        try {
            //发件人
            message.setFrom(new InternetAddress(FROM));
            //收件人
            //TO表示收件人，CC表示抄送，BCC表示秘密抄送
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(mail));
            //主题
            message.setSubject(title);
            //内容
            message.setText(content);
            //发送邮件
            Transport.send(message);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
