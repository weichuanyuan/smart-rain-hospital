package com.wei.service.mapstruct;

import com.wei.entity.Dept;
import com.wei.entity.dto.DeptDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DeptMapper {
    DeptMapper INSTANCE = Mappers.getMapper(DeptMapper.class);

    @Mappings({
            @Mapping(source = "deptCode", target = "code"),
            @Mapping(source = "deptName", target = "name"),
            @Mapping(source = "deptPinyin", target = "pinyin"),
            @Mapping(source = "deptPhone", target = "phone"),
            @Mapping(source = "deptSort", target = "sort"),
            @Mapping(source = "deptIntroduce", target = "introduce"),
            @Mapping(source = "deptAddress", target = "address"),
            @Mapping(source = "deptTypeName", target = "typeName")
    })
    DeptDto  deptToDeptDto(Dept dept);

    List<DeptDto>  deptListToDeptDtoList(List<Dept> deptList);

}
