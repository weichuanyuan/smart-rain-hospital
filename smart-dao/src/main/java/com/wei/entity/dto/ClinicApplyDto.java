package com.wei.entity.dto;

import java.util.Date;

public class ClinicApplyDto {
    /**
     * 申请单号
     */
    private String applyNo;
    /**
     * 申请科室
     */
    private String applyDeptname;
    /**
     * 申请时间
     */
    private Date applyTime;
    /**
     * 执行科室
     */
    private String execDeptname;
    /**
     * 执行时间
     */
    private Date execTime;
    /**
     * 执行项目名称
     */
    private String applyItemName;

    public String getApplyNo() {
        return applyNo;
    }

    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }

    public String getApplyDeptname() {
        return applyDeptname;
    }

    public void setApplyDeptname(String applyDeptname) {
        this.applyDeptname = applyDeptname;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getExecDeptname() {
        return execDeptname;
    }

    public void setExecDeptname(String execDeptname) {
        this.execDeptname = execDeptname;
    }

    public Date getExecTime() {
        return execTime;
    }

    public void setExecTime(Date execTime) {
        this.execTime = execTime;
    }

    public String getApplyItemName() {
        return applyItemName;
    }

    public void setApplyItemName(String applyItemName) {
        this.applyItemName = applyItemName;
    }
}
