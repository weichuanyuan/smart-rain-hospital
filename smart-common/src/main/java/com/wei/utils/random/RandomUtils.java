package com.wei.utils.random;

import com.wei.utils.ApplicationConstants;

import java.util.Random;

public class RandomUtils {
    private static  final String sourceNumber = "0123456789";

    public static String random(int bit){
        if(bit<0){
            return ApplicationConstants.EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int index = 0;index<bit;index++){
            sb.append(  sourceNumber.charAt( random.nextInt(9)));
        }
        return sb.toString();
    }

}
