package com.wei.service.mapstruct;

import com.wei.entity.Employee;
import com.wei.entity.dto.EmployeeDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);

    @Mappings({
            @Mapping(source = "drCode", target = "code"),
            @Mapping(source = "drName", target = "name"),
            @Mapping(source = "drPinyin", target = "pinyin"),
            @Mapping(source = "drDeptCode", target = "deptCode"),
            @Mapping(source = "drDeptName", target = "deptName"),
            @Mapping(source = "drLevelName", target = "levelName"),
            @Mapping(source = "drPhoto", target = "url"),
            @Mapping(source = "drIntroduce", target = "introduce")
    })
    EmployeeDto doctorToDoctorDto(Employee employee);

    List<EmployeeDto> doctorListToDoctorDtoList(List<Employee> employee);
}
