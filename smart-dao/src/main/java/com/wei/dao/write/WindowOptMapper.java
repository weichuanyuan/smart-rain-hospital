package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.Window;

public interface WindowOptMapper extends IBaseOptDao<Window,Integer> {
    int updateWindow(Window window);
}
