package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.Scheduling;

public interface SchedulingOptMapper extends IBaseOptDao<Scheduling,Integer> {
}
