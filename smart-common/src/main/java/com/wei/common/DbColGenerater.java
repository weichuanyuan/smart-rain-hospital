package com.wei.common;

import com.wei.utils.ApplicationConstants;
import com.wei.utils.Stringutils;

public final class DbColGenerater {
    /**
     * 防止被实例化
     */
    private DbColGenerater() {
    }


    /**
     * 数据库字段前缀字符
     */
    private static final String DBCOLPREFIX = "tc";
    /**
     * 数据库字段前缀和下划线
     */
    private static final String DBCOLPREFIXANDUNDERLINE = "tc_";
    /**
     * 数据库表前缀和
     */
    private static final String DBTABLEPREFIX = "tb";

    /**
     * 数据库字段
     *
     * @param field 实体字段
     * @return
     */
    public static String generaterDbColName(String field) {
        if (Stringutils.isEmptyOrWhiteSpace(field)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(DBCOLPREFIXANDUNDERLINE);

        char[] chars = field.toCharArray();
        for (char ch : chars) {
            if (Stringutils.isUpperCaseChar(ch)) {
                sb.append(ApplicationConstants.UNDERLINE).append(Stringutils.toLower(ch));
            } else {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    /**
     * 类字段
     *
     * @param colName 数据库字段
     * @return
     */
    public static String generaterClassFieldName(String colName) {
        if (Stringutils.isEmptyOrWhiteSpace(colName)) {
            return "";
        }
        String source = colName;
        if (Stringutils.startsWithIgnoreCase(colName, DBCOLPREFIXANDUNDERLINE)) {
            source = Stringutils.subStr(colName, DBCOLPREFIXANDUNDERLINE.length());
        }
        StringBuilder sb = new StringBuilder();
        char[] chars = source.toCharArray();
        for (int index = 0; index < chars.length; index++) {
            if (ApplicationConstants.UNDERLINE.equals(Stringutils.toString(chars[index]))) {
                index++;
                if(index<chars.length) {
                    if(1==index) {
                        sb.append(Stringutils.toLower(chars[index]));
                    }else{
                        sb.append(Stringutils.toUpper(chars[index]));
                    }
                }
            } else {
                sb.append(Stringutils.toLower(chars[index]));
            }
        }
        return sb.toString();
    }

    public static String generaterClassTableName(String tbName) {
        if (Stringutils.isEmptyOrWhiteSpace(tbName)) {
            return "";
        }
        String source = tbName;
        if (Stringutils.startsWithIgnoreCase(tbName, DBTABLEPREFIX)) {
            source = Stringutils.subStr(tbName, DBTABLEPREFIX.length());
        }
        StringBuilder sb = new StringBuilder();

        char[] chars = source.toCharArray();

        for (int index = 0; index < chars.length; index++) {
            if (ApplicationConstants.UNDERLINE.equals(Stringutils.toString(chars[index]))) {
                index++;
                if(index<chars.length) {
                    sb.append(Stringutils.toUpper(chars[index]));
                }
            } else {
                sb.append(Stringutils.toLower(chars[index]));
            }
        }

        return sb.toString();
    }
}
