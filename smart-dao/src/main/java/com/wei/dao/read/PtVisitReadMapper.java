package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.PtVisit;

public interface PtVisitReadMapper extends IBaseReadDao<PtVisit,Integer> {

}