package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Register implements Serializable {

    /**
    * 主键
    */
    private Integer regId;

    /**
    * 结算主表id
    */
    private Integer mainId;

    /**
    * 病人就诊id
    */
    private Integer visitId;

    /**
    * 就诊次数
    */
    private Integer visitSeries;

    /**
    * 病人id
    */
    private Integer ptId;

    /**
    * 病人姓名
    */
    private String ptName;

    /**
    * 挂号医生
    */
    private String regDrCode;

    /**
    * 挂号科室
    */
    private String regDeptCode;

    /**
    * 排班主键
    */
    private Integer scheId;

    /**
    * 上午 1 下午 2 晚上 3
    */
    private String apn;

    /**
    * 具体时间
    */
    private String time;

    /**
    * 挂号时间
    */
    private Date regTime;

    /**
    * 挂号状态 1 已挂号 4 已取消
    */
    private String status;

    /**
    * 取消时间
    */
    private Date cancelTime;

    /**
    * 操作员
    */
    private String regOperCode;

    /**
    * 挂号金额
    */
    private BigDecimal fee;

    private Integer schedulingDetailId;

    public Integer getSchedulingDetailId() {
        return schedulingDetailId;
    }

    public void setSchedulingDetailId(Integer schedulingDetailId) {
        this.schedulingDetailId = schedulingDetailId;
    }

    public Integer getRegId() {
        return regId;
    }
    public void setRegId(Integer regId) {
        this.regId = regId;
    }

    public Integer getMainId() {
        return mainId;
    }
    public void setMainId(Integer mainId) {
        this.mainId = mainId;
    }

    public Integer getVisitId() {
        return visitId;
    }
    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public Integer getVisitSeries() {
        return visitSeries;
    }
    public void setVisitSeries(Integer visitSeries) {
        this.visitSeries = visitSeries;
    }

    public Integer getPtId() {
        return ptId;
    }
    public void setPtId(Integer ptId) {
        this.ptId = ptId;
    }

    public String getPtName() {
        return ptName;
    }
    public void setPtName(String ptName) {
        this.ptName = ptName;
    }

    public String getRegDrCode() {
        return regDrCode;
    }
    public void setRegDrCode(String regDrCode) {
        this.regDrCode = regDrCode;
    }

    public String getRegDeptCode() {
        return regDeptCode;
    }
    public void setRegDeptCode(String regDeptCode) {
        this.regDeptCode = regDeptCode;
    }

    public Integer getScheId() {
        return scheId;
    }
    public void setScheId(Integer scheId) {
        this.scheId = scheId;
    }

    public String getApn() {
        return apn;
    }
    public void setApn(String apn) {
        this.apn = apn;
    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    public Date getRegTime() {
        return regTime;
    }
    public void setRegTime(Date regTime) {
        this.regTime = regTime;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCancelTime() {
        return cancelTime;
    }
    public void setCancelTime(Date cancelTime) {
        this.cancelTime = cancelTime;
    }

    public String getRegOperCode() {
        return regOperCode;
    }
    public void setRegOperCode(String regOperCode) {
        this.regOperCode = regOperCode;
    }

    public BigDecimal getFee() {
        return fee;
    }
    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }


    @Override
    public String toString() {
        return "Register{" +
        "regId='" + regId + '\'' +
        "mainId='" + mainId + '\'' +
        "visitId='" + visitId + '\'' +
        "visitSeries='" + visitSeries + '\'' +
        "ptId='" + ptId + '\'' +
        "ptName='" + ptName + '\'' +
        "regDrCode='" + regDrCode + '\'' +
        "regDeptCode='" + regDeptCode + '\'' +
        "scheId='" + scheId + '\'' +
        "apn='" + apn + '\'' +
        "time='" + time + '\'' +
        "regTime='" + regTime + '\'' +
        "status='" + status + '\'' +
        "cancelTime='" + cancelTime + '\'' +
        "regOperCode='" + regOperCode + '\'' +
        "fee='" + fee + '\'' +
    '}';
    }

}
