package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Main implements Serializable {

    /**
    * 主键
    */
    private Integer mainId;

    /**
    * 门诊0 住院1 急诊2 体检 3
    */
    private String chargeType;

    /**
    * 就诊id
    */
    private Integer visitId;

    /**
    * 就诊次数
    */
    private Integer visitSeries;

    /**
    * 病人id
    */
    private Integer ptId;

    /**
    * 发票号
    */
    private String mainInvoice;

    /**
    * 总金额
    */
    private BigDecimal mainFee;

    /**
    * 结算状态 1 已结算 4 已退费
    */
    private String mainStatus;

    /**
    * 结算科室
    */
    private String mainChargeDeptCode;

    /**
    * 结算人编码
    */
    private String mainEmpCode;

    /**
    * 结算场合 R 挂号 C 收费
    */
    private String mainOccType;

    /**
    * 结算时间
    */
    private Date mainTime;

    /**
    * 退费时间
    */
    private Date mainBackTime;

    /**
    * 退费人编码
    */
    private String mainBackEmpCode;

    /**
    * 本次账户支付
    */
    private BigDecimal mainAccount;

    /**
    * 自负
    */
    private BigDecimal mainSelfpay;

    /**
    * 统筹支付
    */
    private BigDecimal mainAccumulate;

    /**
    * 公务员补助
    */
    private BigDecimal mainCivil;

    /**
    * 大病统筹
    */
    private BigDecimal mainSupp;

    /**
    * 补充
    */
    private BigDecimal mainEnterpriseSupp;


    public Integer getMainId() {
        return mainId;
    }
    public void setMainId(Integer mainId) {
        this.mainId = mainId;
    }

    public String getChargeType() {
        return chargeType;
    }
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getVisitId() {
        return visitId;
    }
    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public Integer getVisitSeries() {
        return visitSeries;
    }
    public void setVisitSeries(Integer visitSeries) {
        this.visitSeries = visitSeries;
    }

    public Integer getPtId() {
        return ptId;
    }
    public void setPtId(Integer ptId) {
        this.ptId = ptId;
    }

    public String getMainInvoice() {
        return mainInvoice;
    }
    public void setMainInvoice(String mainInvoice) {
        this.mainInvoice = mainInvoice;
    }

    public BigDecimal getMainFee() {
        return mainFee;
    }
    public void setMainFee(BigDecimal mainFee) {
        this.mainFee = mainFee;
    }

    public String getMainStatus() {
        return mainStatus;
    }
    public void setMainStatus(String mainStatus) {
        this.mainStatus = mainStatus;
    }

    public String getMainChargeDeptCode() {
        return mainChargeDeptCode;
    }
    public void setMainChargeDeptCode(String mainChargeDeptCode) {
        this.mainChargeDeptCode = mainChargeDeptCode;
    }

    public String getMainEmpCode() {
        return mainEmpCode;
    }
    public void setMainEmpCode(String mainEmpCode) {
        this.mainEmpCode = mainEmpCode;
    }

    public String getMainOccType() {
        return mainOccType;
    }
    public void setMainOccType(String mainOccType) {
        this.mainOccType = mainOccType;
    }

    public Date getMainTime() {
        return mainTime;
    }
    public void setMainTime(Date mainTime) {
        this.mainTime = mainTime;
    }

    public Date getMainBackTime() {
        return mainBackTime;
    }
    public void setMainBackTime(Date mainBackTime) {
        this.mainBackTime = mainBackTime;
    }

    public String getMainBackEmpCode() {
        return mainBackEmpCode;
    }
    public void setMainBackEmpCode(String mainBackEmpCode) {
        this.mainBackEmpCode = mainBackEmpCode;
    }

    public BigDecimal getMainAccount() {
        return mainAccount;
    }
    public void setMainAccount(BigDecimal mainAccount) {
        this.mainAccount = mainAccount;
    }

    public BigDecimal getMainSelfpay() {
        return mainSelfpay;
    }
    public void setMainSelfpay(BigDecimal mainSelfpay) {
        this.mainSelfpay = mainSelfpay;
    }

    public BigDecimal getMainAccumulate() {
        return mainAccumulate;
    }
    public void setMainAccumulate(BigDecimal mainAccumulate) {
        this.mainAccumulate = mainAccumulate;
    }

    public BigDecimal getMainCivil() {
        return mainCivil;
    }
    public void setMainCivil(BigDecimal mainCivil) {
        this.mainCivil = mainCivil;
    }

    public BigDecimal getMainSupp() {
        return mainSupp;
    }
    public void setMainSupp(BigDecimal mainSupp) {
        this.mainSupp = mainSupp;
    }

    public BigDecimal getMainEnterpriseSupp() {
        return mainEnterpriseSupp;
    }
    public void setMainEnterpriseSupp(BigDecimal mainEnterpriseSupp) {
        this.mainEnterpriseSupp = mainEnterpriseSupp;
    }


    @Override
    public String toString() {
        return "Main{" +
        "mainId='" + mainId + '\'' +
        "chargeType='" + chargeType + '\'' +
        "visitId='" + visitId + '\'' +
        "visitSeries='" + visitSeries + '\'' +
        "ptId='" + ptId + '\'' +
        "mainInvoice='" + mainInvoice + '\'' +
        "mainFee='" + mainFee + '\'' +
        "mainStatus='" + mainStatus + '\'' +
        "mainChargeDeptCode='" + mainChargeDeptCode + '\'' +
        "mainEmpCode='" + mainEmpCode + '\'' +
        "mainOccType='" + mainOccType + '\'' +
        "mainTime='" + mainTime + '\'' +
        "mainBackTime='" + mainBackTime + '\'' +
        "mainBackEmpCode='" + mainBackEmpCode + '\'' +
        "mainAccount='" + mainAccount + '\'' +
        "mainSelfpay='" + mainSelfpay + '\'' +
        "mainAccumulate='" + mainAccumulate + '\'' +
        "mainCivil='" + mainCivil + '\'' +
        "mainSupp='" + mainSupp + '\'' +
        "mainEnterpriseSupp='" + mainEnterpriseSupp + '\'' +
    '}';
    }

}
