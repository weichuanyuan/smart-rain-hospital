package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.Appointment;
import com.wei.entity.SchedulingDetail;
import org.apache.ibatis.annotations.Param;

public interface SchedulingDetailOptMapper extends IBaseOptDao<SchedulingDetail,Integer> {

    Integer insertAppointment(Appointment appointment);

    Integer updateSchedulingDetailStatus(@Param("sourceId")String sourceId);
    Integer updateSchedulingAmNumber(@Param("sourceId")String sourceId);
    Integer updateSchedulingPmNumber(@Param("sourceId")String sourceId);
    Integer updateSchedulingEveNumber(@Param("sourceId")String sourceId);

    Integer cancelAppointment(@Param("appointId")String appointId);
    Integer updateBreakAppointment(Appointment appointment);

    Integer updateAppointmentStatus(@Param("appointId")String appointId);
}
