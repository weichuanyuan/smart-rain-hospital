package com.wei.web.listener;

import com.wei.quartz.scheduler.StartScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

//@Component
public class StartupListener implements ApplicationListener<ContextRefreshedEvent> {
    private static final Logger logger = LoggerFactory.getLogger(StartupListener.class);

    @Autowired
    private StartScheduler startScheduler;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        logger.info("容器加载完成!");
        try {
            startScheduler.start();
        } catch (Exception e) {
            logger.error("定时任务启动失败!",e);
        }
//        if (contextRefreshedEvent.getApplicationContext().getParent() == null){
//
//        }
    }
}
