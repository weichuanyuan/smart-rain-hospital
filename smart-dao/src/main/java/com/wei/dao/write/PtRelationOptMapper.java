package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.PtRelation;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface PtRelationOptMapper extends IBaseOptDao<PtRelation,Integer> {

    Integer updateCrtFlagByPtid(@Param("ptId") Integer ptId);

    Integer updateCrtFlagByIoNo(Map<String,String> map);

    Integer unBindPatient(Map<String,String> map);

}
