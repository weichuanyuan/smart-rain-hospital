package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.Token;

public interface TokenReadMapper extends IBaseReadDao<Token,String> {

}