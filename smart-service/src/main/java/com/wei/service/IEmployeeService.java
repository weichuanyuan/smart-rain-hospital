package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.Employee;
import com.wei.entity.dto.EmployeeDto;

import java.util.List;

public interface IEmployeeService {

    boolean insert(Employee employee);

    boolean update(Employee employee);

    boolean delete(Integer id);

    boolean batchSava(List<Employee> employeeList);

    Employee getById(Integer id);

    List<Employee> getAll(Page<Employee> page);

    List<EmployeeDto> getDoctorDtoList(String features);

    List<EmployeeDto> getExpertDoctor();

    Integer totalCount();
}