package com.wei.utils.date;

import com.wei.utils.Stringutils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    /** 分 */
    public static final long MINUTE_TTL = 60 * 1000l;
    /** 时 */
    public static final long HOURS_TTL = 60 * 60 * 1000l;
    /** 半天 */
    public static final long HALF_DAY_TTL = 12 * 60 * 60 * 1000l;
    /** 天 */
    public static final long DAY_TTL = 24 * 60 * 60 * 1000l;
    /** 月 */
    public static final long MONTH_TTL = 30 * 24 * 60 * 60 * 1000l;
    /** 时间格式(yyyy-MM-dd) */
    public final static String DATE_PATTERN = "yyyy-MM-dd";
    /** 时间格式(yyyy-MM-dd HH:mm:ss) */
    public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static String format(Date date, String pattern) {
        if(date != null){
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            return df.format(date);
        }
        return null;
    }

    public static String subDay(Date date,Integer days, String pattern){
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.DAY_OF_MONTH, days);
        Date sub = rightNow.getTime();
        return format(sub,pattern);
    }
}

