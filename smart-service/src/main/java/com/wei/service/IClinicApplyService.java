package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.ClinicApply;
import com.wei.entity.ClinicResult;
import com.wei.entity.dto.ClinicApplyDto;

import java.util.List;
import java.util.Map;

public interface IClinicApplyService {

    boolean insert(ClinicApply clinicApply);

    boolean update(ClinicApply clinicApply);

    boolean delete(Integer id);

    boolean batchSava(List<ClinicApply> clinicApplyList);

    ClinicApply getById(Integer id);

    List<ClinicApply> getAll(Page<ClinicApply> page);

    Integer totalCount();

    List<ClinicApplyDto> getApply(Map<String,String> map);

    List<ClinicResult> getApplyResult(String applyNo);
}