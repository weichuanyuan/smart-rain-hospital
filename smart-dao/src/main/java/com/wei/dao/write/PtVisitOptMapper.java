package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.PtVisit;

public interface PtVisitOptMapper extends IBaseOptDao<PtVisit,Integer> {
}
