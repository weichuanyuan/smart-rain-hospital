package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ClinicApply implements Serializable {

    /**
    * 申请id
    */
    private Integer applyId;

    /**
    * 申请单号
    */
    private String applyNo;
    /**
     * 病人ID
     */
    private Integer ptId;
    /**
    * 申请类型 1检查 2检验等
    */
    private String applyType;

    /**
    * 申请项目编码
    */
    private String applyCode;

    /**
    * 申请项目名称
    */
    private String applyName;

    /**
    * 申请科室编码
    */
    private String applyDeptCode;

    /**
    * 申请科室名称
    */
    private String applyDeptName;

    /**
    * 部位
    */
    private String applyPart;

    /**
    * 申请时间
    */
    private Date applyTime;

    /**
    * 申请状态 0 未缴费 1 已发送 2 已接收 4 已拒绝
    */
    private String applyStatus;

    /**
    * 申请医生
    */
    private String applyDrCode;

    /**
    * 预约时间
    */
    private Date applyAppointTime;

    /**
    * 预约操作时间
    */
    private Date applyAppointOptTime;

    /**
    * 预约操作人
    */
    private String applyAppointDrCode;

    /**
    * 预约来源编码
    */
    private String applyAppointUseType;

    /**
    * 预约来源名称
    */
    private String applyAppointUseName;

    /**
    * 执行科室编码
    */
    private String execDeptCode;

    /**
    * 执行科室名称
    */
    private String execDeptName;

    /**
    * 执行状态 0 未执行 1 已执行 2 取消执行
    */
    private String execStatus;

    /**
    * 执行医生
    */
    private String execDrCode;

    /**
    * 执行时间
    */
    private Date execTime;

    /**
    * 发送时间
    */
    private Date sendTime;

    public Integer getPtId() {
        return ptId;
    }

    public void setPtId(Integer ptId) {
        this.ptId = ptId;
    }

    public Integer getApplyId() {
        return applyId;
    }
    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public String getApplyNo() {
        return applyNo;
    }
    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }

    public String getApplyType() {
        return applyType;
    }
    public void setApplyType(String applyType) {
        this.applyType = applyType;
    }

    public String getApplyCode() {
        return applyCode;
    }
    public void setApplyCode(String applyCode) {
        this.applyCode = applyCode;
    }

    public String getApplyName() {
        return applyName;
    }
    public void setApplyName(String applyName) {
        this.applyName = applyName;
    }

    public String getApplyDeptCode() {
        return applyDeptCode;
    }
    public void setApplyDeptCode(String applyDeptCode) {
        this.applyDeptCode = applyDeptCode;
    }

    public String getApplyDeptName() {
        return applyDeptName;
    }
    public void setApplyDeptName(String applyDeptName) {
        this.applyDeptName = applyDeptName;
    }

    public String getApplyPart() {
        return applyPart;
    }
    public void setApplyPart(String applyPart) {
        this.applyPart = applyPart;
    }

    public Date getApplyTime() {
        return applyTime;
    }
    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getApplyStatus() {
        return applyStatus;
    }
    public void setApplyStatus(String applyStatus) {
        this.applyStatus = applyStatus;
    }

    public String getApplyDrCode() {
        return applyDrCode;
    }
    public void setApplyDrCode(String applyDrCode) {
        this.applyDrCode = applyDrCode;
    }

    public Date getApplyAppointTime() {
        return applyAppointTime;
    }
    public void setApplyAppointTime(Date applyAppointTime) {
        this.applyAppointTime = applyAppointTime;
    }

    public Date getApplyAppointOptTime() {
        return applyAppointOptTime;
    }
    public void setApplyAppointOptTime(Date applyAppointOptTime) {
        this.applyAppointOptTime = applyAppointOptTime;
    }

    public String getApplyAppointDrCode() {
        return applyAppointDrCode;
    }
    public void setApplyAppointDrCode(String applyAppointDrCode) {
        this.applyAppointDrCode = applyAppointDrCode;
    }

    public String getApplyAppointUseType() {
        return applyAppointUseType;
    }
    public void setApplyAppointUseType(String applyAppointUseType) {
        this.applyAppointUseType = applyAppointUseType;
    }

    public String getApplyAppointUseName() {
        return applyAppointUseName;
    }
    public void setApplyAppointUseName(String applyAppointUseName) {
        this.applyAppointUseName = applyAppointUseName;
    }

    public String getExecDeptCode() {
        return execDeptCode;
    }
    public void setExecDeptCode(String execDeptCode) {
        this.execDeptCode = execDeptCode;
    }

    public String getExecDeptName() {
        return execDeptName;
    }
    public void setExecDeptName(String execDeptName) {
        this.execDeptName = execDeptName;
    }

    public String getExecStatus() {
        return execStatus;
    }
    public void setExecStatus(String execStatus) {
        this.execStatus = execStatus;
    }

    public String getExecDrCode() {
        return execDrCode;
    }
    public void setExecDrCode(String execDrCode) {
        this.execDrCode = execDrCode;
    }

    public Date getExecTime() {
        return execTime;
    }
    public void setExecTime(Date execTime) {
        this.execTime = execTime;
    }

    public Date getSendTime() {
        return sendTime;
    }
    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }



}
