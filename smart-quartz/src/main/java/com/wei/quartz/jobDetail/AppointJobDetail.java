package com.wei.quartz.jobDetail;

import com.wei.quartz.job.AppointJob;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppointJobDetail {
    @Bean
    public JobDetail getJobDetail(){
        JobDetail job = JobBuilder.newJob(AppointJob.class)
                .withIdentity(AppointJob.class.getName())
                .build();
        return job;
    }
}
