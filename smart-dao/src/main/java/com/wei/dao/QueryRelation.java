package com.wei.dao;


public enum  QueryRelation {
    AND("and"),
    OR("or");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    QueryRelation(String value){
        this.value = value;
    }
}
