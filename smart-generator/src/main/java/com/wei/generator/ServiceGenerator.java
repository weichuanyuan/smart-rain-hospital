package com.wei.generator;

import com.wei.utils.ApplicationConstants;
import com.wei.utils.ResourceUtils;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Map;

@Component("serviceGenerator")
public class ServiceGenerator implements Generator {
    @Override
    public void GeneratorCode(Map<String, Object> map) {
        Writer writer = null;
        try {

            Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
            configuration.setObjectWrapper(new DefaultObjectWrapper(Configuration.VERSION_2_3_28));
            File file = new File(ResourceUtils.getClasspath()+"freemarker");
            configuration.setDirectoryForTemplateLoading(file);

            // service
            Template template = configuration.getTemplate("service.flt", "UTF-8");
            String mapperPath = ResourceUtils.getProjectRootPath()+
                    ApplicationConstants.SLASH+
                    map.get("servicePath").toString() +
                    ApplicationConstants.SLASH+
                    map.get("servicePackageName").toString().replace('.','/')+
                    ApplicationConstants.SLASH+
                    map.get("serviceName").toString() +".java";
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mapperPath),"UTF-8"));
            template.process(map,writer);

            // serviceImpl
            template = configuration.getTemplate("serviceImpl.flt", "UTF-8");
            mapperPath = ResourceUtils.getProjectRootPath()+
                    ApplicationConstants.SLASH+
                    map.get("serviceImplPath").toString() +
                    ApplicationConstants.SLASH+
                    map.get("serviceImplPackageName").toString().replace('.','/')+
                    ApplicationConstants.SLASH+
                    map.get("serviceImplName").toString() +".java";
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mapperPath),"UTF-8"));
            template.process(map,writer);

            writer.flush();
            writer.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
