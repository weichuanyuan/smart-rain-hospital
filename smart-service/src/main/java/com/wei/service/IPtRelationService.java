package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.PtRelation;

import java.util.List;

public interface IPtRelationService {

    boolean insert(PtRelation ptRelation);

    boolean update(PtRelation ptRelation);

    boolean delete(Integer id);

    boolean batchSava(List<PtRelation> ptRelationList);

    PtRelation getById(Integer id);

    List<PtRelation> getAll(Page<PtRelation> page);

    Integer totalCount();
}