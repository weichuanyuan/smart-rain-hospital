package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class WechatAuth implements Serializable {

    /**
    * 微信小程序openid
    */
    private String openId;

    /**
    * 微信小程序unionid
    */
    private String unionId;

    /**
    * 微信小程序sessionkey
    */
    private String sessionKey;

    /**
    * 系统token GUID
    */
    private String token;

    /**
    * 01 微信小程序
    */
    private String type;

    /**
    * 生成时间
    */
    private Date modDt;


    public String getOpenId() {
        return openId;
    }
    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getUnionId() {
        return unionId;
    }
    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getSessionKey() {
        return sessionKey;
    }
    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Date getModDt() {
        return modDt;
    }
    public void setModDt(Date modDt) {
        this.modDt = modDt;
    }


    @Override
    public String toString() {
        return "WechatAuth{" +
        "openId='" + openId + '\'' +
        "unionId='" + unionId + '\'' +
        "sessionKey='" + sessionKey + '\'' +
        "token='" + token + '\'' +
        "type='" + type + '\'' +
        "modDt='" + modDt + '\'' +
    '}';
    }

}
