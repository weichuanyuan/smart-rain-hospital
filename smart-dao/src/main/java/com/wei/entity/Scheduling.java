package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Scheduling implements Serializable {

    /**
    * 主键
    */
    private Integer scheId;

    /**
    * 排班类型,治疗 4/检查 3/检验 2/挂号 1
    */
    private String scheTypeCode;

    /**
    * 上午使用状态 Y 再用 N 停用
    */
    private String scheAmState;

    /**
    * 下午使用状态 Y 再用 N 停用
    */
    private String schePmState;

    /**
    * 晚上使用状态 Y 再用 N 停用
    */
    private String scheEveState;

    /**
    * 排班时间
    */
    private Date scheDate;

    /**
    * 排班医生
    */
    private String scheDrCode;

    /**
    * 排班科室
    */
    private String scheDeptCode;

    /**
    * 排班备注
    */
    private String scheRemark;

    /**
    * 挂号类型 
    */
    private String scheRegTypeCode;

    /**
    * 挂号类型名称
    */
    private String scheRegTypeName;

    /**
    * 排班医生职称
    */
    private String drLevelCode;

    /**
    * 上午号源数量
    */
    private Integer scheAmNumber;

    /**
    * 上午已用号源数量
    */
    private Integer scheAmUsenumber;

    /**
    * 下午号源数量
    */
    private Integer schePmNumber;

    /**
    * 下午已用号源数量
    */
    private Integer schePmUsenumber;

    /**
    * 晚上号源数量
    */
    private Integer scheEveNumber;

    /**
    * 晚上已用号源数量
    */
    private Integer scheEveUsenumber;

    /**
    * 操作人
    */
    private String scheCreateCode;

    /**
    * 操作时间
    */
    private Date scheCreateDate;

    /**
    * 修改时间
    */
    private Date scheModifyDate;

    /**
    * 上午开始时间
    */
    private String scheAmStart;

    /**
    * 上午结束时间
    */
    private String scheAmEnd;

    /**
    * 下午开始时间
    */
    private String schePmStart;

    /**
    * 下午结束时间
    */
    private String schePmEnd;

    /**
    * 晚上开始时间
    */
    private String scheEveStart;

    /**
    * 晚上结束时间
    */
    private String scheEveEnd;
    public Integer getScheId() {
        return scheId;
    }
    public void setScheId(Integer scheId) {
        this.scheId = scheId;
    }

    public String getScheTypeCode() {
        return scheTypeCode;
    }
    public void setScheTypeCode(String scheTypeCode) {
        this.scheTypeCode = scheTypeCode;
    }

    public String getScheAmState() {
        return scheAmState;
    }
    public void setScheAmState(String scheAmState) {
        this.scheAmState = scheAmState;
    }

    public String getSchePmState() {
        return schePmState;
    }
    public void setSchePmState(String schePmState) {
        this.schePmState = schePmState;
    }

    public String getScheEveState() {
        return scheEveState;
    }
    public void setScheEveState(String scheEveState) {
        this.scheEveState = scheEveState;
    }

    public Date getScheDate() {
        return scheDate;
    }
    public void setScheDate(Date scheDate) {
        this.scheDate = scheDate;
    }

    public String getScheDrCode() {
        return scheDrCode;
    }
    public void setScheDrCode(String scheDrCode) {
        this.scheDrCode = scheDrCode;
    }

    public String getScheDeptCode() {
        return scheDeptCode;
    }
    public void setScheDeptCode(String scheDeptCode) {
        this.scheDeptCode = scheDeptCode;
    }

    public String getScheRemark() {
        return scheRemark;
    }
    public void setScheRemark(String scheRemark) {
        this.scheRemark = scheRemark;
    }

    public String getScheRegTypeCode() {
        return scheRegTypeCode;
    }
    public void setScheRegTypeCode(String scheRegTypeCode) {
        this.scheRegTypeCode = scheRegTypeCode;
    }

    public String getScheRegTypeName() {
        return scheRegTypeName;
    }
    public void setScheRegTypeName(String scheRegTypeName) {
        this.scheRegTypeName = scheRegTypeName;
    }

    public String getDrLevelCode() {
        return drLevelCode;
    }
    public void setDrLevelCode(String drLevelCode) {
        this.drLevelCode = drLevelCode;
    }

    public Integer getScheAmNumber() {
        return scheAmNumber;
    }
    public void setScheAmNumber(Integer scheAmNumber) {
        this.scheAmNumber = scheAmNumber;
    }

    public Integer getScheAmUsenumber() {
        return scheAmUsenumber;
    }
    public void setScheAmUsenumber(Integer scheAmUsenumber) {
        this.scheAmUsenumber = scheAmUsenumber;
    }

    public Integer getSchePmNumber() {
        return schePmNumber;
    }
    public void setSchePmNumber(Integer schePmNumber) {
        this.schePmNumber = schePmNumber;
    }

    public Integer getSchePmUsenumber() {
        return schePmUsenumber;
    }
    public void setSchePmUsenumber(Integer schePmUsenumber) {
        this.schePmUsenumber = schePmUsenumber;
    }

    public Integer getScheEveNumber() {
        return scheEveNumber;
    }
    public void setScheEveNumber(Integer scheEveNumber) {
        this.scheEveNumber = scheEveNumber;
    }

    public Integer getScheEveUsenumber() {
        return scheEveUsenumber;
    }
    public void setScheEveUsenumber(Integer scheEveUsenumber) {
        this.scheEveUsenumber = scheEveUsenumber;
    }

    public String getScheCreateCode() {
        return scheCreateCode;
    }
    public void setScheCreateCode(String scheCreateCode) {
        this.scheCreateCode = scheCreateCode;
    }

    public Date getScheCreateDate() {
        return scheCreateDate;
    }
    public void setScheCreateDate(Date scheCreateDate) {
        this.scheCreateDate = scheCreateDate;
    }

    public Date getScheModifyDate() {
        return scheModifyDate;
    }
    public void setScheModifyDate(Date scheModifyDate) {
        this.scheModifyDate = scheModifyDate;
    }

    public String getScheAmStart() {
        return scheAmStart;
    }
    public void setScheAmStart(String scheAmStart) {
        this.scheAmStart = scheAmStart;
    }

    public String getScheAmEnd() {
        return scheAmEnd;
    }
    public void setScheAmEnd(String scheAmEnd) {
        this.scheAmEnd = scheAmEnd;
    }

    public String getSchePmStart() {
        return schePmStart;
    }
    public void setSchePmStart(String schePmStart) {
        this.schePmStart = schePmStart;
    }

    public String getSchePmEnd() {
        return schePmEnd;
    }
    public void setSchePmEnd(String schePmEnd) {
        this.schePmEnd = schePmEnd;
    }

    public String getScheEveStart() {
        return scheEveStart;
    }
    public void setScheEveStart(String scheEveStart) {
        this.scheEveStart = scheEveStart;
    }

    public String getScheEveEnd() {
        return scheEveEnd;
    }
    public void setScheEveEnd(String scheEveEnd) {
        this.scheEveEnd = scheEveEnd;
    }


    @Override
    public String toString() {
        return "Scheduling{" +
        "scheId='" + scheId + '\'' +
        "scheTypeCode='" + scheTypeCode + '\'' +
        "scheAmState='" + scheAmState + '\'' +
        "schePmState='" + schePmState + '\'' +
        "scheEveState='" + scheEveState + '\'' +
        "scheDate='" + scheDate + '\'' +
        "scheDrCode='" + scheDrCode + '\'' +
        "scheDeptCode='" + scheDeptCode + '\'' +
        "scheRemark='" + scheRemark + '\'' +
        "scheRegTypeCode='" + scheRegTypeCode + '\'' +
        "scheRegTypeName='" + scheRegTypeName + '\'' +
        "drLevelCode='" + drLevelCode + '\'' +
        "scheAmNumber='" + scheAmNumber + '\'' +
        "scheAmUsenumber='" + scheAmUsenumber + '\'' +
        "schePmNumber='" + schePmNumber + '\'' +
        "schePmUsenumber='" + schePmUsenumber + '\'' +
        "scheEveNumber='" + scheEveNumber + '\'' +
        "scheEveUsenumber='" + scheEveUsenumber + '\'' +
        "scheCreateCode='" + scheCreateCode + '\'' +
        "scheCreateDate='" + scheCreateDate + '\'' +
        "scheModifyDate='" + scheModifyDate + '\'' +
        "scheAmStart='" + scheAmStart + '\'' +
        "scheAmEnd='" + scheAmEnd + '\'' +
        "schePmStart='" + schePmStart + '\'' +
        "schePmEnd='" + schePmEnd + '\'' +
        "scheEveStart='" + scheEveStart + '\'' +
        "scheEveEnd='" + scheEveEnd + '\'' +
    '}';
    }

}
