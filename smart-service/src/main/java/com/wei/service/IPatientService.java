package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.Patient;
import com.wei.entity.dto.PatientDto;

import java.util.List;
import java.util.Map;

public interface IPatientService {

    boolean insert(Patient patient);

    boolean update(Patient patient);

    boolean delete(Integer id);

    boolean batchSava(List<Patient> patientList);

    Patient getById(Integer id);

    List<Patient> getAll(Page<Patient> page);

    Integer totalCount();

    PatientDto createPatient(PatientDto patient);

    List<PatientDto> getBindPatientDtoList();

    List<PatientDto> fitCrtPatient(String ioNo);

    List<PatientDto> unBindPatient(String ioNo);

    List<PatientDto> bindPatient(PatientDto patientDto);

    String getIoNo(Map<String,String> map);
}