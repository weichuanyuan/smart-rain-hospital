package com.wei.entity.vo;

import com.wei.entity.Patient;

public class PatientVo extends Patient {
    /**
     * 是否是当前患者
     */
    private String crtFlag;

    private String contrast;

    public String getContrast() {
        return contrast;
    }

    public void setContrast(String contrast) {
        this.contrast = contrast;
    }

    public String getCrtFlag() {
        return crtFlag;
    }

    public void setCrtFlag(String crtFlag) {
        this.crtFlag = crtFlag;
    }
}
