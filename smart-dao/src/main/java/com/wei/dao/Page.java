package com.wei.dao;

import java.io.Serializable;

public class Page<T extends Serializable> extends QueryWrapper<T> {

    /**
     * 每页显示多少 每页记录数
     */
    private int pageSize;
    /**
     * 第几页 ---页号
     */
    private int pageNo;
    /**
     * 总记录数
     */
    private int total;

    @Override
    public String toString() {
        return "Page{" +
                "pageSize=" + pageSize +
                ", pageNo=" + pageNo +
                ", total=" + total +
                '}';
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
