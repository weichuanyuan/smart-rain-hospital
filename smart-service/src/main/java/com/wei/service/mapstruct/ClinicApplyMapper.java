package com.wei.service.mapstruct;


import com.wei.entity.ClinicApply;
import com.wei.entity.dto.ClinicApplyDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClinicApplyMapper {
    ClinicApplyMapper INSTANCE = Mappers.getMapper(ClinicApplyMapper.class);

    @Mappings({
            @Mapping(source = "applyNo", target = "applyNo"),
            @Mapping(source = "applyDeptName", target = "applyDeptname"),
            @Mapping(source = "applyTime", target = "applyTime"),
            @Mapping(source = "execDeptName", target = "execDeptname"),
            @Mapping(source = "execTime", target = "execTime"),
            @Mapping(source = "applyName", target = "applyItemName")
    })
    ClinicApplyDto clinicApplyToClinicApplyDto(ClinicApply clinicApply) ;

    List<ClinicApplyDto> clinicApplyListToClinicApplyDtoList(List<ClinicApply> clinicApplyList);
}
