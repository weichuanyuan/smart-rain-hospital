package com.wei.service.mapstruct;

import com.wei.entity.Patient;
import com.wei.entity.dto.PatientDto;
import com.wei.entity.vo.PatientVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PatientMapper {
    PatientMapper INSTANCE = Mappers.getMapper(PatientMapper.class);

    PatientDto patientToPatientDto(Patient patient);
    @Mappings({
            @Mapping(source = "ioNo", target = "ptIoNo"),
            @Mapping(source = "name", target = "ptName"),
            @Mapping(source = "cardno", target = "ptCardid"),
            @Mapping(source = "cardtype", target = "ptCardtype"),
            @Mapping(source = "phone", target = "ptPhone"),
            @Mapping(source = "sex", target = "ptSex"),
            @Mapping(source = "province", target = "ptProvinceName"),
            @Mapping(source = "city", target = "ptCityName"),
            @Mapping(source = "county", target = "ptCountyName"),
            @Mapping(source = "addrDetail", target = "ptTown"),
            @Mapping(source = "child", target = "ptChild"),
            @Mapping(source = "guardianCardtype", target = "ptGuardianCardtype"),
            @Mapping(source = "guardianCardno", target = "ptGuardianCardno")
    })
    Patient patientDtoToPatient(PatientDto patient);

    List<PatientDto> patientListToPatientDtoList(List<Patient> patient);

    @Mappings({
            @Mapping(source = "ptIoNo", target = "ioNo"),
            @Mapping(source = "ptName", target = "name"),
            @Mapping(source = "ptCardid", target = "cardno"),
            @Mapping(source = "ptCardtype", target = "cardtype"),
            @Mapping(source = "ptPhone", target = "phone"),
            @Mapping(source = "ptSex", target = "sex"),
            @Mapping(source = "ptProvinceName", target = "province"),
            @Mapping(source = "ptCityName", target = "city"),
            @Mapping(source = "ptCountyName", target = "county"),
            @Mapping(source = "ptTown", target = "addrDetail"),
            @Mapping(source = "ptChild", target = "child"),
            @Mapping(source = "ptGuardianCardtype", target = "guardianCardtype"),
            @Mapping(source = "ptGuardianCardno", target = "guardianCardno"),
            @Mapping(source = "crtFlag", target = "crtFlag")
    })
    PatientDto patientToPatient(PatientVo patient);

    List<PatientDto> patientVoListToPatientDtoList(List<PatientVo> patientVoList);


}
