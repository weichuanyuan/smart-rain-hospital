package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SchedulingDetail implements Serializable {

    /**
    * 主键
    */
    private Integer regId;

    /**
    * 排班表id
    */
    private Integer regScheId;

    /**
    * 当前号
    */
    private Integer regNo;

    /**
    * 上午 1 下午 2 晚上 3
    */
    private String regApe;

    /**
    * 使用状态
    */
    private String regState;

    /**
    * 时间,例如11:30
    */
    private String regTime;

    /**
    * 号源类型
    */
    private String regType;

    /**
    * 号源使用备注信息
    */
    private String regRemark;

    /**
    * 号源使用来源编码 微信小程序/官网/支付宝/app/客户端等
    */
    private String regUseType;

    /**
    * 号源使用来源名称 微信小程序/官网/支付宝/app/客户端等
    */
    private String regUseName;


    public Integer getRegId() {
        return regId;
    }
    public void setRegId(Integer tbRegId) {
        this.regId = tbRegId;
    }

    public Integer getRegScheId() {
        return regScheId;
    }
    public void setRegScheId(Integer regScheId) {
        this.regScheId = regScheId;
    }

    public Integer getRegNo() {
        return regNo;
    }
    public void setRegNo(Integer regNo) {
        this.regNo = regNo;
    }

    public String getRegApe() {
        return regApe;
    }
    public void setRegApe(String regApe) {
        this.regApe = regApe;
    }

    public String getRegState() {
        return regState;
    }
    public void setRegState(String regState) {
        this.regState = regState;
    }

    public String getRegTime() {
        return regTime;
    }
    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }

    public String getRegType() {
        return regType;
    }
    public void setRegType(String regType) {
        this.regType = regType;
    }

    public String getRegRemark() {
        return regRemark;
    }
    public void setRegRemark(String regRemark) {
        this.regRemark = regRemark;
    }

    public String getRegUseType() {
        return regUseType;
    }
    public void setRegUseType(String regUseType) {
        this.regUseType = regUseType;
    }

    public String getRegUseName() {
        return regUseName;
    }
    public void setRegUseName(String regUseName) {
        this.regUseName = regUseName;
    }


    @Override
    public String toString() {
        return "SchedulingDetail{" +
        "regId='" + regId + '\'' +
        "regScheId='" + regScheId + '\'' +
        "regNo='" + regNo + '\'' +
        "regApe='" + regApe + '\'' +
        "regState='" + regState + '\'' +
        "regTime='" + regTime + '\'' +
        "regType='" + regType + '\'' +
        "regRemark='" + regRemark + '\'' +
        "regUseType='" + regUseType + '\'' +
        "regUseName='" + regUseName + '\'' +
    '}';
    }

}
