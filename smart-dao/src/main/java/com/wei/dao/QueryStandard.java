package com.wei.dao;

import java.io.Serializable;

public class QueryStandard implements Serializable {
    @Override
    public String toString() {
        return "QueryStandard{" +
                "field='" + field + '\'' +
                ", value='" + value + '\'' +
                ", condition=" + condition +
                ", relation=" + relation +
                '}';
    }

    /**
     * 字段
     */
    private String field;
    /**
     * 值
     */
    private String value;

    /**
     * 条件
     */
    private QueryCondition condition;
    /**
     * 与上一个条件的关系
     * 第一个条件就是与where 1=1的关系
     */
    private QueryRelation relation;

    public QueryRelation getRelation() {
        return relation;
    }

    public void setRelation(QueryRelation relation) {
        this.relation = relation;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public QueryCondition getCondition() {
        return condition;
    }

    public void setCondition(QueryCondition condition) {
        this.condition = condition;
    }

}
