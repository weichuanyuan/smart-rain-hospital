package com.wei.service.impl;

import com.wei.dao.Page;
import com.wei.dao.read.EmployeeReadMapper;
import com.wei.dao.write.EmployeeOptMapper;
import com.wei.entity.Employee;
import com.wei.entity.dto.EmployeeDto;
import com.wei.service.IEmployeeService;
import com.wei.service.mapstruct.EmployeeMapper;
import com.wei.service.wrapper.RedisCacheWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EmployeeServiceImpl implements IEmployeeService {
    @Autowired
    private EmployeeOptMapper employeeOptMapper;
    @Autowired
    private EmployeeReadMapper employeeReadMapper;

    public boolean insert(Employee employee) {
        return employeeOptMapper.insert(employee) > 0;
    }

    public boolean update(Employee employee) {
        return employeeOptMapper.update(employee) > 0;
    }

    public boolean delete(Integer id) {
        return employeeOptMapper.delete(id) > 0;
    }

    public boolean batchSava(List<Employee> employeeList) {
        return employeeOptMapper.batchSava(employeeList) > 0;
    }

    public Employee getById(Integer id) {
        return employeeReadMapper.getById(id);
    }

    public Integer totalCount() {
        return employeeReadMapper.totalCount();
    }

    public List<Employee> getAll(Page<Employee> page) {
        return employeeReadMapper.getAll(page);
    }

    public List<EmployeeDto> getDoctorDtoList(String features){
        // 门诊医生
        List<Employee> list = employeeReadMapper.getDoctorDtoList(features);
        return EmployeeMapper.INSTANCE.doctorListToDoctorDtoList(list);
    }

    public List<EmployeeDto> getExpertDoctor(){
        List<Employee> list = employeeReadMapper.getExpertDoctor();
        return EmployeeMapper.INSTANCE.doctorListToDoctorDtoList(list);
    }
}
