package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.Charge;
import com.wei.entity.DicChargeFee;
import com.wei.entity.Prescription;
import com.wei.entity.vo.PrescriptionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PrescriptionReadMapper extends IBaseReadDao<Prescription,Integer> {
    List<PrescriptionVo> getPrescriptionVoList(Map<String,String> map);

    List<Charge> getChargeByPrescriptionId(@Param("prescriptionId") Integer prescriptionId);

    List<PrescriptionVo> getPrescriptionVoListByIds(String[] ids);

    List<DicChargeFee> getDicChargeFeeListByRegCode(@Param("regCode")String regCode);
}