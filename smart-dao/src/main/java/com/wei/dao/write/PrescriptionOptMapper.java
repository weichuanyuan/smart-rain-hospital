package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.Charge;
import com.wei.entity.Main;
import com.wei.entity.Prescription;
import com.wei.entity.Register;

public interface PrescriptionOptMapper extends IBaseOptDao<Prescription,Integer> {

   Integer savaMain(Main main);

   Integer savaCharge(Charge charge);
   Integer savaRegister(Register register);

   int updatePrescriptionChargeStatus(Prescription prescription);

   Integer updateChargeStatus(Charge charge);

}
