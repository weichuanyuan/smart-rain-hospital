package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.HosDic;

public interface HosDicReadMapper extends IBaseReadDao<HosDic,Integer> {

}