package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.Patient;

public interface PatientOptMapper extends IBaseOptDao<Patient,Integer> {
}
