package com.wei.entity;

import java.util.Date;

public class ClinicResult {
    /**
     * 申请单号
     */
    private String applyNo;

    /**
     * 结果时间
     */
    private Date resultTime;

    /**
     * 项目编码
     */
    private String itemCode;

    /**
     * 项目名称
     */
    private String itemName;

    /**
     * 参考值
     */
    private String itemReference;

    /**
     * 定性
     */
    private String itemQualitative;

    /**
     * 单位
     */
    private String itemUnit;

    /**
     * 所见
     */
    private String itemList;

    /**
     * 所得
     */
    private String itemResult;


    public String getApplyNo() {
        return applyNo;
    }
    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }

    public Date getResultTime() {
        return resultTime;
    }
    public void setResultTime(Date resultTime) {
        this.resultTime = resultTime;
    }

    public String getItemCode() {
        return itemCode;
    }
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemReference() {
        return itemReference;
    }
    public void setItemReference(String itemReference) {
        this.itemReference = itemReference;
    }

    public String getItemQualitative() {
        return itemQualitative;
    }
    public void setItemQualitative(String itemQualitative) {
        this.itemQualitative = itemQualitative;
    }

    public String getItemUnit() {
        return itemUnit;
    }
    public void setItemUnit(String itemUnit) {
        this.itemUnit = itemUnit;
    }

    public String getItemList() {
        return itemList;
    }
    public void setItemList(String itemList) {
        this.itemList = itemList;
    }

    public String getItemResult() {
        return itemResult;
    }
    public void setItemResult(String itemResult) {
        this.itemResult = itemResult;
    }


    @Override
    public String toString() {
        return "TcClinicResult{" +
                "applyNo='" + applyNo + '\'' +
                "resultTime='" + resultTime + '\'' +
                "itemCode='" + itemCode + '\'' +
                "itemName='" + itemName + '\'' +
                "itemReference='" + itemReference + '\'' +
                "itemQualitative='" + itemQualitative + '\'' +
                "itemUnit='" + itemUnit + '\'' +
                "itemList='" + itemList + '\'' +
                "itemResult='" + itemResult + '\'' +
                '}';
    }
}
