package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.Scheduling;
import com.wei.entity.vo.SchedulingVo;

import java.util.List;
import java.util.Map;

public interface ISchedulingService {

    boolean insert(Scheduling scheduling);

    boolean update(Scheduling scheduling);

    boolean delete(Integer id);

    boolean batchSava(List<Scheduling> schedulingList);

    Scheduling getById(Integer id);

    List<Scheduling> getAll(Page<Scheduling> page);

    Integer totalCount();

    List<SchedulingVo> getDoctorSchedulingList(Map<String,String> maps);
}