package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.Dept;

public interface DeptOptMapper extends IBaseOptDao<Dept,Integer> {
}
