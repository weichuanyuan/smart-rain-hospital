package com.wei.service.impl;

import com.wei.dao.Page;
import com.wei.dao.read.PtRelationReadMapper;
import com.wei.dao.write.PtRelationOptMapper;
import com.wei.entity.PtRelation;
import com.wei.service.IPtRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PtRelationServiceImpl implements IPtRelationService {
    @Autowired
    private PtRelationOptMapper ptRelationOptMapper;
    @Autowired
    private PtRelationReadMapper ptRelationReadMapper;

    public boolean insert(PtRelation ptRelation) {
        return ptRelationOptMapper.insert(ptRelation) > 0;
    }

    public boolean update(PtRelation ptRelation) {
        return ptRelationOptMapper.update(ptRelation) > 0;
    }

    public boolean delete(Integer id) {
        return ptRelationOptMapper.delete(id) > 0;
    }

    public boolean batchSava(List<PtRelation> ptRelationList) {
        return ptRelationOptMapper.batchSava(ptRelationList) > 0;
    }

    public PtRelation getById(Integer id) {
        return ptRelationReadMapper.getById(id);
    }

    public Integer totalCount() {
        return ptRelationReadMapper.totalCount();
    }

    public List<PtRelation> getAll(Page<PtRelation> page) {
        return ptRelationReadMapper.getAll(page);
    }
}
