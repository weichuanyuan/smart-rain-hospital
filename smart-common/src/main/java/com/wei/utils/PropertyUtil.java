package com.wei.utils;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil {

    public static final String EMPTY = "";

    public static Properties getPropertiesResource(String url){
        try {
            Properties property = new Properties();
            InputStream inputStream = PropertyUtil.class
                    .getResourceAsStream(url);
            property.load(inputStream);

            return property;
        }
        catch (IOException ex){
            ex.printStackTrace();
            return null;
        }
    }

    public static  String getValue(Properties property,String key){
        if(property==null){
            return EMPTY;
        }
        return property.getProperty(key).trim();
    }
}
