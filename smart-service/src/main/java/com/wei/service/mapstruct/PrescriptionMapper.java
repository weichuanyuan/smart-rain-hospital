package com.wei.service.mapstruct;

import com.wei.entity.dto.PrescriptionDto;
import com.wei.entity.vo.PrescriptionVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PrescriptionMapper {
    PrescriptionMapper INSTANCE = Mappers.getMapper(PrescriptionMapper.class);
    @Mappings({
            @Mapping(source = "prescriptionId", target = "prescriptionId"),
            @Mapping(source = "prescriptionTime", target = "prescriptionTime"),
            @Mapping(source = "prescriptionDeptName", target = "prescriptionDeptName"),
            @Mapping(source = "prescriptionDrname", target = "prescriptionDrname"),
            @Mapping(source = "chargeAmount", target = "chargeFee"),
            @Mapping(source = "prescriptionTypeName", target = "prescriptionTypeName"),
            @Mapping(source = "guideAddr", target = "guideAddress"),
            @Mapping(source = "mainId", target = "mainId"),
    })
    PrescriptionDto prescriptionVoToPrescriptionDto(PrescriptionVo prescriptionVo);

    List<PrescriptionDto> prescriptionVoListToPrescriptionDtoList(List<PrescriptionVo> prescriptionVoList);
}
