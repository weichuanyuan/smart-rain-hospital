package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.WechatAuth;

public interface WechatAuthOptMapper extends IBaseOptDao<WechatAuth,String> {
}
