package com.wei.generator;

import com.wei.utils.ApplicationConstants;
import com.wei.utils.ResourceUtils;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Map;
@Component("mapperXmlGenerator")
public class MapperXmlGenerator implements Generator{

    @Override
    public void GeneratorCode(Map<String, Object> map) {
        Writer writer = null;
        try {

            Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
            configuration.setObjectWrapper(new DefaultObjectWrapper(Configuration.VERSION_2_3_28));
            File file = new File(ResourceUtils.getClasspath()+"freemarker");
            configuration.setDirectoryForTemplateLoading(file);

            // 读 mapper xml
            Template template = configuration.getTemplate("readXmlMapper.flt", "UTF-8");
            String mapperPath = ResourceUtils.getProjectRootPath()+
                    ApplicationConstants.SLASH+
                    map.get("readXmlPath").toString() +
                    ApplicationConstants.SLASH+
                    map.get("readMapperName").toString() +".xml";
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mapperPath),"UTF-8"));
            template.process(map,writer);

            // 读 mapper extend xml
            template = configuration.getTemplate("readExtendsXmlMapper.flt", "UTF-8");
            mapperPath = ResourceUtils.getProjectRootPath()+
                    ApplicationConstants.SLASH+
                    map.get("readExtendXmlPath").toString() +
                    ApplicationConstants.SLASH+
                    map.get("readExtendMapperName").toString() +".xml";
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mapperPath),"UTF-8"));
            template.process(map,writer);

            // 写 mapper xml
            template = configuration.getTemplate("writeXmlMapper.flt", "UTF-8");
            mapperPath = ResourceUtils.getProjectRootPath()+
                    ApplicationConstants.SLASH+
                    map.get("writeXmlPath").toString() +
                    ApplicationConstants.SLASH+
                    map.get("writeMapperName").toString() +".xml";
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mapperPath),"UTF-8"));
            template.process(map,writer);

            // 写 mapper extend xml
            template = configuration.getTemplate("writeExtendsXmlMapper.flt", "UTF-8");
            mapperPath = ResourceUtils.getProjectRootPath()+
                    ApplicationConstants.SLASH+
                    map.get("writeExtendXmlPath").toString() +
                    ApplicationConstants.SLASH+
                    map.get("writeExtendMapperName").toString() +".xml";
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mapperPath),"UTF-8"));
            template.process(map,writer);


            writer.flush();
            writer.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
