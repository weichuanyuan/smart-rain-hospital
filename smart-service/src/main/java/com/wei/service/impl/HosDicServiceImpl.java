package com.wei.service.impl;

import com.wei.dao.Page;
import com.wei.dao.read.HosDicReadMapper;
import com.wei.dao.write.HosDicOptMapper;
import com.wei.entity.HosDic;
import com.wei.service.IHosDicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class HosDicServiceImpl implements IHosDicService {
    @Autowired
    private HosDicOptMapper hosDicOptMapper;
    @Autowired
    private HosDicReadMapper hosDicReadMapper;

    public boolean insert(HosDic hosDic) {
        return hosDicOptMapper.insert(hosDic) > 0;
    }

    public boolean update(HosDic hosDic) {
        return hosDicOptMapper.update(hosDic) > 0;
    }

    public boolean delete(Integer id) {
        return hosDicOptMapper.delete(id) > 0;
    }

    public boolean batchSava(List<HosDic> hosDicList) {
        return hosDicOptMapper.batchSava(hosDicList) > 0;
    }

    public HosDic getById(Integer id) {
        return hosDicReadMapper.getById(id);
    }

    public Integer totalCount() {
        return hosDicReadMapper.totalCount();
    }

    public List<HosDic> getAll(Page<HosDic> page) {
        return hosDicReadMapper.getAll(page);
    }
}
