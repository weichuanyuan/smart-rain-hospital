package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.Employee;

public interface EmployeeOptMapper extends IBaseOptDao<Employee,Integer> {
}
