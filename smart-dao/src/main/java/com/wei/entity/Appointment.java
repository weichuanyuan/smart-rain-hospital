package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Appointment implements Serializable {

    /**
    * 预约主键
    */
    private Integer appointId;

    /**
    * 排班id
    */
    private Integer appointScheId;

    /**
    * 预约姓名
    */
    private String appointName;

    /**
    * 患者id
    */
    private Integer appoinitPtId;

    /**
    * 预约门诊号
    */
    private String appointIoNo;

    /**
    * 预约号,可以自己产生,也可以第三方产生
    */
    private String appointNo;

    /**
    * 预约手机号码
    */
    private String appointPhone;

    /**
    * 预约医生
    */
    private String appointDrCode;

    /**
    * 预约科室
    */
    private String appointDeptCode;

    /**
    * 上午 1 下午 2 晚上 3
    */
    private String appointApn;

    /**
    * 预约时间
    */
    private Date appointDate;

    /**
    * 具体时间 11:25
    */
    private String appointTime;

    /**
    * 预约状态 取消 已预约 已使用
    */
    private String appointState;

    /**
    * 预约来源
    */
    private String appointUseType;

    /**
    * 预约来源名称
    */
    private String appointUseName;

    /**
    * 操作时间
    */
    private Date appointCreateDate;

    /**
    * 取消时间
    */
    private Date appointCancelDate;

    /**
    * 是否爽约
    */
    private String appointBlacklist;

    /**
    * 预约操作人
    */
    private String appointOptCode;

    /**
    * 取消预约操作人
    */
    private String appointCancalOptCode;

    private Integer schedulingDetailId;


    public Integer getSchedulingDetailId() {
        return schedulingDetailId;
    }

    public void setSchedulingDetailId(Integer schedulingDetailId) {
        this.schedulingDetailId = schedulingDetailId;
    }


    public Integer getAppointScheId() {
        return appointScheId;
    }
    public void setAppointScheId(Integer appointScheId) {
        this.appointScheId = appointScheId;
    }

    public String getAppointName() {
        return appointName;
    }
    public void setAppointName(String appointName) {
        this.appointName = appointName;
    }

    public Integer getAppoinitPtId() {
        return appoinitPtId;
    }
    public void setAppoinitPtId(Integer appoinitPtId) {
        this.appoinitPtId = appoinitPtId;
    }

    public String getAppointIoNo() {
        return appointIoNo;
    }
    public void setAppointIoNo(String appointIoNo) {
        this.appointIoNo = appointIoNo;
    }

    public String getAppointNo() {
        return appointNo;
    }
    public void setAppointNo(String appointNo) {
        this.appointNo = appointNo;
    }

    public String getAppointPhone() {
        return appointPhone;
    }
    public void setAppointPhone(String appointPhone) {
        this.appointPhone = appointPhone;
    }

    public String getAppointDrCode() {
        return appointDrCode;
    }
    public void setAppointDrCode(String appointDrCode) {
        this.appointDrCode = appointDrCode;
    }

    public String getAppointDeptCode() {
        return appointDeptCode;
    }
    public void setAppointDeptCode(String appointDeptCode) {
        this.appointDeptCode = appointDeptCode;
    }

    public String getAppointApn() {
        return appointApn;
    }
    public void setAppointApn(String appointApn) {
        this.appointApn = appointApn;
    }

    public Date getAppointDate() {
        return appointDate;
    }
    public void setAppointDate(Date appointDate) {
        this.appointDate = appointDate;
    }

    public String getAppointTime() {
        return appointTime;
    }
    public void setAppointTime(String appointTime) {
        this.appointTime = appointTime;
    }

    public String getAppointState() {
        return appointState;
    }
    public void setAppointState(String appointState) {
        this.appointState = appointState;
    }

    public String getAppointUseType() {
        return appointUseType;
    }
    public void setAppointUseType(String appointUseType) {
        this.appointUseType = appointUseType;
    }

    public String getAppointUseName() {
        return appointUseName;
    }
    public void setAppointUseName(String appointUseName) {
        this.appointUseName = appointUseName;
    }

    public Date getAppointCreateDate() {
        return appointCreateDate;
    }
    public void setAppointCreateDate(Date appointCreateDate) {
        this.appointCreateDate = appointCreateDate;
    }

    public Integer getAppointId() {
        return appointId;
    }

    public void setAppointId(Integer appointId) {
        this.appointId = appointId;
    }

    public Date getAppointCancelDate() {
        return appointCancelDate;
    }
    public void setAppointCancelDate(Date appointCancelDate) {
        this.appointCancelDate = appointCancelDate;
    }

    public String getAppointBlacklist() {
        return appointBlacklist;
    }
    public void setAppointBlacklist(String appointBlacklist) {
        this.appointBlacklist = appointBlacklist;
    }

    public String getAppointOptCode() {
        return appointOptCode;
    }
    public void setAppointOptCode(String appointOptCode) {
        this.appointOptCode = appointOptCode;
    }

    public String getAppointCancalOptCode() {
        return appointCancalOptCode;
    }
    public void setAppointCancalOptCode(String appointCancalOptCode) {
        this.appointCancalOptCode = appointCancalOptCode;
    }



}
