package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.HosDic;

import java.util.List;

public interface IHosDicService {

    boolean insert(HosDic hosDic);

    boolean update(HosDic hosDic);

    boolean delete(Integer id);

    boolean batchSava(List<HosDic> hosDicList);

    HosDic getById(Integer id);

    List<HosDic> getAll(Page<HosDic> page);

    Integer totalCount();
}