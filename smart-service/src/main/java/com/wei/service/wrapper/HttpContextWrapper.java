package com.wei.service.wrapper;

import com.wei.common.ApplicationCacheDefinition;
import com.wei.dao.read.PatientReadMapper;
import com.wei.entity.Patient;
import com.wei.entity.vo.PatientVo;
import com.wei.utils.ApplicationConstants;
import com.wei.utils.Stringutils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
public class HttpContextWrapper {

    @Autowired
    private RedisCacheWrapper redisCacheWrapper;

    @Autowired
    private PatientReadMapper patientReadMapper;


    /**
     * 获取token
     *
     * @return
     */
    public String getToken() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();

        String token = request.getHeader(ApplicationConstants.TOKEN);
        if(token==null){
            return "";
        }
        return token;
    }

    /**
     * 获取HttpServletRequest对象
     *
     * @return
     */
    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
    }

    public Patient CurrentPatient() {
        String token = getToken();
        if(redisCacheWrapper.hasKey(Stringutils.format(ApplicationCacheDefinition.PATIENT,HttpContextWrapper.class.toString(),token))){
            return (Patient)(redisCacheWrapper.get(Stringutils.format(ApplicationCacheDefinition.PATIENT,HttpContextWrapper.class.toString(),token)).get());
        }
        Patient patient = patientReadMapper.getPatientByToken(token);
        redisCacheWrapper.put(Stringutils.format(ApplicationCacheDefinition.PATIENT,HttpContextWrapper.class.toString(),token),patient);
        return patient;
    }

    public List<PatientVo> BindPatientList(){
        String token = getToken();
        if(redisCacheWrapper.hasKey(Stringutils.format(ApplicationCacheDefinition.PATIENTLIST,HttpContextWrapper.class.toString(),token))){
            return (List<PatientVo>)(redisCacheWrapper.get(Stringutils.format(ApplicationCacheDefinition.PATIENTLIST,HttpContextWrapper.class.toString(),token)).get());
        }
        List<PatientVo> patientList = patientReadMapper.getBindPatientVoList(token);
        if(patientList.stream().noneMatch(m->ApplicationConstants.Y.equals( m.getCrtFlag()))){
            for (int index=0;index<patientList.size();index++){
                patientList.get(index).setCrtFlag(patientList.get(index).getContrast());
            }
        }
        redisCacheWrapper.put(Stringutils.format(ApplicationCacheDefinition.PATIENTLIST,HttpContextWrapper.class.toString(),token),patientList);
        return patientList;
    }

}
