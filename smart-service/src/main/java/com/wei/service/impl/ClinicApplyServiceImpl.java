package com.wei.service.impl;

import com.wei.dao.Page;
import com.wei.dao.read.ClinicApplyReadMapper;
import com.wei.dao.read.PatientReadMapper;
import com.wei.dao.write.ClinicApplyOptMapper;
import com.wei.entity.ClinicApply;
import com.wei.entity.ClinicResult;
import com.wei.entity.Patient;
import com.wei.entity.dto.ClinicApplyDto;
import com.wei.service.IClinicApplyService;
import com.wei.service.mapstruct.ClinicApplyMapperImpl;
import com.wei.utils.ExceptionUtils;
import com.wei.utils.Stringutils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ClinicApplyServiceImpl implements IClinicApplyService {
    @Autowired
    private ClinicApplyOptMapper clinicApplyOptMapper;
    @Autowired
    private ClinicApplyReadMapper clinicApplyReadMapper;
    @Autowired
    private PatientReadMapper patientReadMapper;

    public boolean insert(ClinicApply clinicApply) {
        return clinicApplyOptMapper.insert(clinicApply) > 0;
    }

    public boolean update(ClinicApply clinicApply) {
        return clinicApplyOptMapper.update(clinicApply) > 0;
    }

    public boolean delete(Integer id) {
        return clinicApplyOptMapper.delete(id) > 0;
    }

    public boolean batchSava(List<ClinicApply> clinicApplyList) {
        return clinicApplyOptMapper.batchSava(clinicApplyList) > 0;
    }

    public ClinicApply getById(Integer id) {
        return clinicApplyReadMapper.getById(id);
    }

    public Integer totalCount() {
        return clinicApplyReadMapper.totalCount();
    }

    public List<ClinicApply> getAll(Page<ClinicApply> page) {
        return clinicApplyReadMapper.getAll(page);
    }

    public List<ClinicApplyDto> getApply(Map<String, String> map) {
        String ioNo = map.get("ioNo");
        if (Stringutils.isEmpty(ioNo)) {
            throw ExceptionUtils.mpeValidator("%s", "门诊号不能为空!");
        }
        Patient patient = patientReadMapper.getByIoNo(ioNo);
        if (patient == null) {
            throw ExceptionUtils.mpeValidator("%s", "门诊号不存在!");
        }
        map.put("ptId", patient.getPtId().toString());
        List<ClinicApply> list = clinicApplyReadMapper.getApply(map);
        return ClinicApplyMapperImpl.INSTANCE.clinicApplyListToClinicApplyDtoList(list);
    }

    public List<ClinicResult> getApplyResult(String applyNo) {
        return clinicApplyReadMapper.getApplyResult(applyNo);
    }
}
