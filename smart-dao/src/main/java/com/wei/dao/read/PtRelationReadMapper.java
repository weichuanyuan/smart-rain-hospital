package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.PtRelation;

import java.util.Map;

public interface PtRelationReadMapper extends IBaseReadDao<PtRelation,Integer> {
    PtRelation getByIdAndRelationId(Map<String,String> map);
}