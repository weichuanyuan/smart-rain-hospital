package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.Dept;

import java.util.List;

public interface DeptReadMapper extends IBaseReadDao<Dept,Integer> {
    List<Dept> getFamousDeptList();
}