package com.wei.web.configurer;


import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableWebMvc
@PropertySource("classpath:swagger.properties")
// 访问 http://localhost:8080/swagger-ui.html
public class SwaggerConfig{
    private static final Logger log = LoggerFactory.getLogger(SwaggerConfig.class);

    // 生产环境这个变量设置成false，禁止初始化文档
    @Value("${swagger.init}")
    private boolean initSwagger;
    // swagger标题
    @Value("${swagger.titile}")
    private String apiTitile;
    // swagger说明
    @Value("${swagger.description}")
    private String apiDescription;
    // swagger 版本
    @Value("${swagger.version}")
    private String apiVersion;

    @Bean
    public Docket createRestApi() {
        log.info("开始加载Swagger2...");
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(initSwagger)
                .select()
                // 扫描指定包
                .apis(RequestHandlerSelectors.basePackage("com.wei.controller"))
                // 扫描指定注解
                //.any() 扫描全部
                //.none() 都不扫描
                //.withClassAnnotation() 扫描类上注解      参数是一个注解的反射对象
                //.withMethodAnnotation() 扫描方法上的注解  比如RestController
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(apiTitile)
                .description(apiDescription)
                .version(apiVersion)
                .contact(new Contact("wcy","https://mail.qq.com/","329274634@qq.com"))
                .build();
    }
}
