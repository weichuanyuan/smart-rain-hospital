package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Window implements Serializable,Comparable<Window> {

    /**
    * 主键
    */
    private Integer windowId;

    /**
    * 科室
    */
    private String windowDept;

    /**
    * 窗口
    */
    private String window;

    /**
    * 员工工号
    */
    private String windowEmp;

    /**
    * 处方数量
    */
    private Integer windowNumber;

    /**
    * 在用标志 Y/N
    */
    private String windowStatus;

    /**
    * 最新时间
    */
    private Date windowTime;

    /**
    * 创建时间
    */
    private Date windowCrtTime;


    public Integer getWindowId() {
        return windowId;
    }
    public void setWindowId(Integer windowId) {
        this.windowId = windowId;
    }

    public String getWindowDept() {
        return windowDept;
    }
    public void setWindowDept(String windowDept) {
        this.windowDept = windowDept;
    }

    public String getWindow() {
        return window;
    }
    public void setWindow(String window) {
        this.window = window;
    }

    public String getWindowEmp() {
        return windowEmp;
    }
    public void setWindowEmp(String windowEmp) {
        this.windowEmp = windowEmp;
    }

    public Integer getWindowNumber() {
        return windowNumber;
    }
    public void setWindowNumber(Integer windowNumber) {
        this.windowNumber = windowNumber;
    }

    public String getWindowStatus() {
        return windowStatus;
    }
    public void setWindowStatus(String windowStatus) {
        this.windowStatus = windowStatus;
    }

    public Date getWindowTime() {
        return windowTime;
    }
    public void setWindowTime(Date windowTime) {
        this.windowTime = windowTime;
    }

    public Date getWindowCrtTime() {
        return windowCrtTime;
    }
    public void setWindowCrtTime(Date windowCrtTime) {
        this.windowCrtTime = windowCrtTime;
    }


    @Override
    public String toString() {
        return "Window{" +
        "windowId='" + windowId + '\'' +
        "windowDept='" + windowDept + '\'' +
        "window='" + window + '\'' +
        "windowEmp='" + windowEmp + '\'' +
        "windowNumber='" + windowNumber + '\'' +
        "windowStatus='" + windowStatus + '\'' +
        "windowTime='" + windowTime + '\'' +
        "windowCrtTime='" + windowCrtTime + '\'' +
    '}';
    }

    @Override
    public int compareTo(Window o) {
        int cnt = this.windowNumber-o.getWindowNumber();
        if(cnt==0) {return this.windowTime.compareTo(o.windowTime);}
        return cnt;
    }
}
