package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Prescription implements Serializable {

    /**
    * 处方号
    */
    private Integer prescriptionId;

    /**
    * 处方时间
    */
    private Date prescriptionTime;

    /**
    * 就诊id
    */
    private Integer visitId;
    /**
     * 就诊次数
     */
    private Integer visitSeries;

    /**
    * 门诊0 住院1 急诊2 体检 3
    */
    private String visitType;

    /**
    * 病人id
    */
    private Integer ptId;

    /**
    * 处方类型 见字典A7 诊疗分类
    */
    private String prescriptionType;

    /**
    * 开单科室
    */
    private String prescriptionDeptCode;
    /**
     * 开单医生
     */
    private String prescriptionDrcode;

    /**
    * 处方有效状态 1 有效 0 失效
    */
    private String prescriptionStatus;

    /**
    * 执行科室
    */
    private String execDeptCode;

    /**
    * 收费状态 0 未收费 1 已收费 4 已退费
    */
    private String chargeStatus;

    /**
    * 指引地址
    */
    private String guideAddr;

    /**
    * 窗口号
    */
    private String window;

    /**
    * 执行时间
    */
    private Date execTime;

    /**
    * 诊断编码
    */
    private String diagnoseCode;

    /**
    * 诊断名称
    */
    private String diagnoseName;

    /**
    * 执行状态  0 未执行 1 已执行 4 取消执行
    */
    private String execStatus;

    /**
    * 数量
    */
    private Integer prescriptionCount;

    /**
    * 中药贴数
    */
    private Integer setnumber;

    /**
    * 处方备注
    */
    private String remark;

    /**
    * 中药用法 自煎 代煎 膏方 散剂等
    */
    private String jyMethod;

    /**
    * 代煎方法 代煎卡内容
    */
    private String dyCard;
    /**
     * 处方总金额
     */
    private BigDecimal chargeAmount;
    private Date chargeTime;

    public Date getChargeTime() {
        return chargeTime;
    }

    public void setChargeTime(Date chargeTime) {
        this.chargeTime = chargeTime;
    }

    private Integer mainId;

    public Integer getMainId() {
        return mainId;
    }

    public void setMainId(Integer mainId) {
        this.mainId = mainId;
    }

    public Integer getVisitSeries() {
        return visitSeries;
    }

    public void setVisitSeries(Integer visitSeries) {
        this.visitSeries = visitSeries;
    }

    public BigDecimal getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(BigDecimal chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public Integer getPrescriptionId() {
        return prescriptionId;
    }
    public void setPrescriptionId(Integer prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public Date getPrescriptionTime() {
        return prescriptionTime;
    }
    public void setPrescriptionTime(Date prescriptionTime) {
        this.prescriptionTime = prescriptionTime;
    }

    public Integer getVisitId() {
        return visitId;
    }
    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public String getVisitType() {
        return visitType;
    }
    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public Integer getPtId() {
        return ptId;
    }
    public void setPtId(Integer ptId) {
        this.ptId = ptId;
    }

    public String getPrescriptionType() {
        return prescriptionType;
    }
    public void setPrescriptionType(String prescriptionType) {
        this.prescriptionType = prescriptionType;
    }

    public String getPrescriptionDeptCode() {
        return prescriptionDeptCode;
    }
    public void setPrescriptionDeptCode(String prescriptionDeptCode) {
        this.prescriptionDeptCode = prescriptionDeptCode;
    }

    public String getPrescriptionStatus() {
        return prescriptionStatus;
    }
    public void setPrescriptionStatus(String prescriptionStatus) {
        this.prescriptionStatus = prescriptionStatus;
    }

    public String getExecDeptCode() {
        return execDeptCode;
    }
    public void setExecDeptCode(String execDeptCode) {
        this.execDeptCode = execDeptCode;
    }

    public String getChargeStatus() {
        return chargeStatus;
    }
    public void setChargeStatus(String chargeStatus) {
        this.chargeStatus = chargeStatus;
    }

    public String getGuideAddr() {
        return guideAddr;
    }
    public void setGuideAddr(String guideAddr) {
        this.guideAddr = guideAddr;
    }

    public String getWindow() {
        return window;
    }
    public void setWindow(String window) {
        this.window = window;
    }

    public Date getExecTime() {
        return execTime;
    }
    public void setExecTime(Date execTime) {
        this.execTime = execTime;
    }

    public String getDiagnoseCode() {
        return diagnoseCode;
    }
    public void setDiagnoseCode(String diagnoseCode) {
        this.diagnoseCode = diagnoseCode;
    }

    public String getDiagnoseName() {
        return diagnoseName;
    }
    public void setDiagnoseName(String diagnoseName) {
        this.diagnoseName = diagnoseName;
    }

    public String getExecStatus() {
        return execStatus;
    }
    public void setExecStatus(String execStatus) {
        this.execStatus = execStatus;
    }

    public Integer getPrescriptionCount() {
        return prescriptionCount;
    }
    public void setPrescriptionCount(Integer prescriptionCount) {
        this.prescriptionCount = prescriptionCount;
    }

    public Integer getSetnumber() {
        return setnumber;
    }
    public void setSetnumber(Integer setnumber) {
        this.setnumber = setnumber;
    }

    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getJyMethod() {
        return jyMethod;
    }
    public void setJyMethod(String jyMethod) {
        this.jyMethod = jyMethod;
    }

    public String getDyCard() {
        return dyCard;
    }
    public void setDyCard(String dyCard) {
        this.dyCard = dyCard;
    }

    public String getPrescriptionDrcode() {
        return prescriptionDrcode;
    }

    public void setPrescriptionDrcode(String prescriptionDrcode) {
        this.prescriptionDrcode = prescriptionDrcode;
    }
}
