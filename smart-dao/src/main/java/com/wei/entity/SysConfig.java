package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SysConfig implements Serializable {

    /**
    * 主键
    */
    private Integer id;

    /**
    * 编码
    */
    private String sysCode;

    /**
    * 备注
    */
    private String sysRemark;

    /**
    * int类型配置
    */
    private Integer sysNumber;

    /**
    * decimal类型配置
    */
    private BigDecimal sysDecimal;

    /**
    * datetime类型配置
    */
    private Date sysDate;

    /**
    * 字符类型配置
    */
    private String sysStr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getSysRemark() {
        return sysRemark;
    }

    public void setSysRemark(String sysRemark) {
        this.sysRemark = sysRemark;
    }

    public Integer getSysNumber() {
        return sysNumber;
    }

    public void setSysNumber(Integer sysNumber) {
        this.sysNumber = sysNumber;
    }

    public BigDecimal getSysDecimal() {
        return sysDecimal;
    }

    public void setSysDecimal(BigDecimal sysDecimal) {
        this.sysDecimal = sysDecimal;
    }

    public Date getSysDate() {
        return sysDate;
    }

    public void setSysDate(Date sysDate) {
        this.sysDate = sysDate;
    }

    public String getSysStr() {
        return sysStr;
    }

    public void setSysStr(String sysStr) {
        this.sysStr = sysStr;
    }
}
