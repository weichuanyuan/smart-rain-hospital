package com.wei.service.impl;

import com.wei.dao.Page;
import com.wei.dao.read.TokenReadMapper;
import com.wei.dao.write.TokenOptMapper;
import com.wei.entity.Token;
import com.wei.service.ITokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TokenServiceImpl implements ITokenService {
    @Autowired
    private TokenOptMapper tokenOptMapper;
    @Autowired
    private TokenReadMapper tokenReadMapper;

    public boolean insert(Token token) {
        return tokenOptMapper.insert(token) > 0;
    }

    public boolean update(Token token) {
        return tokenOptMapper.update(token) > 0;
    }

    public boolean delete(String id) {
        return tokenOptMapper.delete(id) > 0;
    }

    public boolean batchSava(List<Token> tokenList) {
        return tokenOptMapper.batchSava(tokenList) > 0;
    }

    public Token getById(String id) {
        return tokenReadMapper.getById(id);
    }

    public Integer totalCount() {
        return tokenReadMapper.totalCount();
    }

    public List<Token> getAll(Page<Token> page) {
        return tokenReadMapper.getAll(page);
    }
}
