package com.wei.service.impl;

import com.wei.dao.Page;
import com.wei.dao.read.SysConfigReadMapper;
import com.wei.dao.write.SysConfigOptMapper;
import com.wei.entity.SysConfig;
import com.wei.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SysConfigServiceImpl implements ISysConfigService {
    @Autowired
    private SysConfigOptMapper sysConfigOptMapper;
    @Autowired
    private SysConfigReadMapper sysConfigReadMapper;

    public boolean insert(SysConfig sysConfig) {
        return sysConfigOptMapper.insert(sysConfig) > 0;
    }

    public boolean update(SysConfig sysConfig) {
        return sysConfigOptMapper.update(sysConfig) > 0;
    }

    public boolean delete(Integer id) {
        return sysConfigOptMapper.delete(id) > 0;
    }

    public boolean batchSava(List<SysConfig> sysConfigList) {
        return sysConfigOptMapper.batchSava(sysConfigList) > 0;
    }

    public SysConfig getById(Integer id) {
        return sysConfigReadMapper.getById(id);
    }

    public Integer totalCount() {
        return sysConfigReadMapper.totalCount();
    }

    @Cacheable(value = "redis", key="#root.targetClass + #root.methodName + #page.pageNo + #page.pageSize + #page.queryStandardList + #page.sortList")
    public List<SysConfig> getAll(Page<SysConfig> page) {
        return sysConfigReadMapper.getAll(page);
    }
}
