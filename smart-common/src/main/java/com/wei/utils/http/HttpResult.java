package com.wei.utils.http;

public class HttpResult {
    public HttpResult(){}

    public HttpResult(Integer statusCode,String content){
        this.statusCode = statusCode;
        this.content = content;
    }

    /**
     * 状态码
     */
    private Integer statusCode;
    /**
     * 返回结果
     */
    private String content;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
