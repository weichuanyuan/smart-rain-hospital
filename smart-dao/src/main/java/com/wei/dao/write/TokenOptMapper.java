package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.Token;

public interface TokenOptMapper extends IBaseOptDao<Token,String> {
}
