package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.SysConfig;

public interface SysConfigOptMapper extends IBaseOptDao<SysConfig,Integer> {

    int increment(SysConfig sysConfig);

}
