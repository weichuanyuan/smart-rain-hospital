package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Charge implements Serializable {

    /**
    * 费用id
    */
    private Integer chargeId;

    /**
    * 门诊0 住院1 急诊2 体检 3
    */
    private String chargeType;

    /**
    * 就诊id
    */
    private Integer visitId;

    /**
    * 就诊次数
    */
    private Integer visitSeries;

    /**
    * 病人id
    */
    private Integer ptId;

    /**
    * 处方号
    */
    private Integer prescriptionId;

    /**
    * 费用分类 见字典A2
    */
    private String type;

    /**
    * 费用编码
    */
    private String chargeCode;

    /**
    * 费用名称
    */
    private String chargeName;

    /**
    * 规格
    */
    private String feeSpecification;

    /**
    * 销售单位
    */
    private String feeUnitCode;

    /**
    * 销售单位名称
    */
    private String feeUnitName;

    /**
    * 剂量单位
    */
    private String feeDoseUnit;

    /**
    * 包装单位
    */
    private String feePackunit;

    /**
    * 单价
    */
    private BigDecimal fee;
    /**
     * 数量
     */
    private BigDecimal chargeAmount;

    /**
    * 总价
    */
    private BigDecimal feeSum;

    /**
    * 发票总表id
    */
    private Integer mainId;

    /**
    * 结算状态 0 未结算 1 已结算 4 已退费
    */
    private String feeStatus;

    /**
    * 开单科室
    */
    private String applyDeptCode;

    /**
    * 执行科室
    */
    private String execDeptCode;

    /**
    * 费用产生时间
    */
    private Date chargeDate;

    /**
    * 开单时间
    */
    private Date applyTime;

    /**
    * 执行时间
    */
    private Date execTime;

    /**
    * 执行状态 0 未执行 1 已执行 4 已取消
    */
    private String execStatus;

    /**
    * 结算时间
    */
    private Date chargeTime;

    public BigDecimal getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(BigDecimal chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public Integer getChargeId() {
        return chargeId;
    }
    public void setChargeId(Integer chargeId) {
        this.chargeId = chargeId;
    }

    public String getChargeType() {
        return chargeType;
    }
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getVisitId() {
        return visitId;
    }
    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public Integer getVisitSeries() {
        return visitSeries;
    }
    public void setVisitSeries(Integer visitSeries) {
        this.visitSeries = visitSeries;
    }

    public Integer getPtId() {
        return ptId;
    }
    public void setPtId(Integer ptId) {
        this.ptId = ptId;
    }

    public Integer getPrescriptionId() {
        return prescriptionId;
    }
    public void setPrescriptionId(Integer prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getChargeCode() {
        return chargeCode;
    }
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    public String getChargeName() {
        return chargeName;
    }
    public void setChargeName(String chargeName) {
        this.chargeName = chargeName;
    }

    public String getFeeSpecification() {
        return feeSpecification;
    }
    public void setFeeSpecification(String feeSpecification) {
        this.feeSpecification = feeSpecification;
    }

    public String getFeeUnitCode() {
        return feeUnitCode;
    }
    public void setFeeUnitCode(String feeUnitCode) {
        this.feeUnitCode = feeUnitCode;
    }

    public String getFeeUnitName() {
        return feeUnitName;
    }
    public void setFeeUnitName(String feeUnitName) {
        this.feeUnitName = feeUnitName;
    }

    public String getFeeDoseUnit() {
        return feeDoseUnit;
    }
    public void setFeeDoseUnit(String feeDoseUnit) {
        this.feeDoseUnit = feeDoseUnit;
    }

    public String getFeePackunit() {
        return feePackunit;
    }
    public void setFeePackunit(String feePackunit) {
        this.feePackunit = feePackunit;
    }

    public BigDecimal getFee() {
        return fee;
    }
    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getFeeSum() {
        return feeSum;
    }
    public void setFeeSum(BigDecimal feeSum) {
        this.feeSum = feeSum;
    }

    public Integer getMainId() {
        return mainId;
    }
    public void setMainId(Integer mainId) {
        this.mainId = mainId;
    }

    public String getFeeStatus() {
        return feeStatus;
    }
    public void setFeeStatus(String feeStatus) {
        this.feeStatus = feeStatus;
    }

    public String getApplyDeptCode() {
        return applyDeptCode;
    }
    public void setApplyDeptCode(String applyDeptCode) {
        this.applyDeptCode = applyDeptCode;
    }

    public String getExecDeptCode() {
        return execDeptCode;
    }
    public void setExecDeptCode(String execDeptCode) {
        this.execDeptCode = execDeptCode;
    }

    public Date getChargeDate() {
        return chargeDate;
    }
    public void setChargeDate(Date chargeDate) {
        this.chargeDate = chargeDate;
    }

    public Date getApplyTime() {
        return applyTime;
    }
    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public Date getExecTime() {
        return execTime;
    }
    public void setExecTime(Date execTime) {
        this.execTime = execTime;
    }

    public String getExecStatus() {
        return execStatus;
    }
    public void setExecStatus(String execStatus) {
        this.execStatus = execStatus;
    }

    public Date getChargeTime() {
        return chargeTime;
    }
    public void setChargeTime(Date chargeTime) {
        this.chargeTime = chargeTime;
    }


    @Override
    public String toString() {
        return "Charge{" +
        "chargeId='" + chargeId + '\'' +
        "chargeType='" + chargeType + '\'' +
        "visitId='" + visitId + '\'' +
        "visitSeries='" + visitSeries + '\'' +
        "ptId='" + ptId + '\'' +
        "prescriptionId='" + prescriptionId + '\'' +
        "type='" + type + '\'' +
        "chargeCode='" + chargeCode + '\'' +
        "chargeName='" + chargeName + '\'' +
        "feeSpecification='" + feeSpecification + '\'' +
        "feeUnitCode='" + feeUnitCode + '\'' +
        "feeUnitName='" + feeUnitName + '\'' +
        "feeDoseUnit='" + feeDoseUnit + '\'' +
        "feePackunit='" + feePackunit + '\'' +
        "fee='" + fee + '\'' +
        "feeSum='" + feeSum + '\'' +
        "mainId='" + mainId + '\'' +
        "feeStatus='" + feeStatus + '\'' +
        "applyDeptCode='" + applyDeptCode + '\'' +
        "execDeptCode='" + execDeptCode + '\'' +
        "chargeDate='" + chargeDate + '\'' +
        "applyTime='" + applyTime + '\'' +
        "execTime='" + execTime + '\'' +
        "execStatus='" + execStatus + '\'' +
        "chargeTime='" + chargeTime + '\'' +
    '}';
    }

}
