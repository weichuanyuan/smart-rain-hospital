package com.wei.generator;

import com.wei.utils.ApplicationConstants;
import com.wei.utils.ResourceUtils;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;

@Component("entityGenerator")
public class EntityGenerator implements Generator {

    @Override
    public void GeneratorCode(Map<String,Object> map) {
        Writer writer = null;
        try {

            Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
            configuration.setObjectWrapper(new DefaultObjectWrapper(Configuration.VERSION_2_3_28));
            File file = new File(ResourceUtils.getClasspath()+"freemarker");
            configuration.setDirectoryForTemplateLoading(file);
            Template template = configuration.getTemplate("entity.flt", "UTF-8");

            String entityPath = ResourceUtils.getProjectRootPath()+
                    ApplicationConstants.SLASH+
                    map.get("entityPath").toString() +
                    ApplicationConstants.SLASH+
                    map.get("packageName").toString().replace('.','/')+
                    ApplicationConstants.SLASH+
                    map.get("entityName").toString() +".java";
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(entityPath),"UTF-8"));


            template.process(map,writer);
            writer.flush();
            writer.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
