package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.Token;

import java.util.List;

public interface ITokenService {

    boolean insert(Token token);

    boolean update(Token token);

    boolean delete(String id);

    boolean batchSava(List<Token> tokenList);

    Token getById(String id);

    List<Token> getAll(Page<Token> page);

    Integer totalCount();
}