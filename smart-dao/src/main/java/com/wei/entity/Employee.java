package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Employee implements Serializable {

    /**
     * id
     */
    private Integer drId;

    /**
     * 编码
     */
    private String drCode;

    /**
     * 姓名
     */
    private String drName;

    /**
     * 医生拼音吗
     */
    private String drPinyin;

    /**
     * 密码
     */
    private String password;

    /**
     * 身份证号
     */
    private String drCardid;

    /**
     * 所在科室编码
     */
    private String drDeptCode;

    /**
     * 所在科室名称
     */
    private String drDeptName;

    /**
     * 职称编码
     */
    private String drLevelCode;

    /**
     * 职称名称
     */
    private String drLevelName;

    /**
     * 职位编码
     */
    private String drPositionCode;

    /**
     * 职位名称
     */
    private String drPositionName;

    /**
     * 照片地址
     */
    private String drPhoto;

    /**
     * 电话
     */
    private String drPhone;

    /**
     * email
     */
    private String drEmail;

    /**
     * 性别
     */
    private String drSex;

    /**
     * 生日
     */
    private Date drBirthday;

    /**
     * 学历
     */
    private String drEdu;

    /**
     * 国家
     */
    private String drCountry;

    /**
     * 民族
     */
    private String drNation;

    /**
     * 籍贯
     */
    private String drNativePlace;

    /**
     * 使用状态
     */
    private String drState;

    /**
     * 医生介绍
     */
    private String drIntroduce;

    /**
     * 医生擅长
     */
    private String drGood;
    /**
     * 专家编码
     */
    private String drExpertCode;
    /**
     * 专家名称
     */
    private String drExpertName;


    public Integer getDrId() {
        return drId;
    }
    public void setDrId(Integer drId) {
        this.drId = drId;
    }

    public String getDrCode() {
        return drCode;
    }
    public void setDrCode(String drCode) {
        this.drCode = drCode;
    }

    public String getDrName() {
        return drName;
    }
    public void setDrName(String drName) {
        this.drName = drName;
    }

    public String getDrPinyin() {
        return drPinyin;
    }
    public void setDrPinyin(String drPinyin) {
        this.drPinyin = drPinyin;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getDrCardid() {
        return drCardid;
    }
    public void setDrCardid(String drCardid) {
        this.drCardid = drCardid;
    }

    public String getDrDeptCode() {
        return drDeptCode;
    }
    public void setDrDeptCode(String drDeptCode) {
        this.drDeptCode = drDeptCode;
    }

    public String getDrDeptName() {
        return drDeptName;
    }
    public void setDrDeptName(String drDeptName) {
        this.drDeptName = drDeptName;
    }

    public String getDrLevelCode() {
        return drLevelCode;
    }
    public void setDrLevelCode(String drLevelCode) {
        this.drLevelCode = drLevelCode;
    }

    public String getDrLevelName() {
        return drLevelName;
    }
    public void setDrLevelName(String drLevelName) {
        this.drLevelName = drLevelName;
    }

    public String getDrPositionCode() {
        return drPositionCode;
    }
    public void setDrPositionCode(String drPositionCode) {
        this.drPositionCode = drPositionCode;
    }

    public String getDrPositionName() {
        return drPositionName;
    }
    public void setDrPositionName(String drPositionName) {
        this.drPositionName = drPositionName;
    }

    public String getDrPhoto() {
        return drPhoto;
    }
    public void setDrPhoto(String drPhoto) {
        this.drPhoto = drPhoto;
    }

    public String getDrPhone() {
        return drPhone;
    }
    public void setDrPhone(String drPhone) {
        this.drPhone = drPhone;
    }

    public String getDrEmail() {
        return drEmail;
    }
    public void setDrEmail(String drEmail) {
        this.drEmail = drEmail;
    }

    public String getDrSex() {
        return drSex;
    }
    public void setDrSex(String drSex) {
        this.drSex = drSex;
    }

    public Date getDrBirthday() {
        return drBirthday;
    }
    public void setDrBirthday(Date drBirthday) {
        this.drBirthday = drBirthday;
    }

    public String getDrEdu() {
        return drEdu;
    }
    public void setDrEdu(String drEdu) {
        this.drEdu = drEdu;
    }

    public String getDrCountry() {
        return drCountry;
    }
    public void setDrCountry(String drCountry) {
        this.drCountry = drCountry;
    }

    public String getDrNation() {
        return drNation;
    }
    public void setDrNation(String drNation) {
        this.drNation = drNation;
    }

    public String getDrNativePlace() {
        return drNativePlace;
    }
    public void setDrNativePlace(String drNativePlace) {
        this.drNativePlace = drNativePlace;
    }

    public String getDrState() {
        return drState;
    }
    public void setDrState(String drState) {
        this.drState = drState;
    }

    public String getDrIntroduce() {
        return drIntroduce;
    }
    public void setDrIntroduce(String drIntroduce) {
        this.drIntroduce = drIntroduce;
    }

    public String getDrGood() {
        return drGood;
    }
    public void setDrGood(String drGood) {
        this.drGood = drGood;
    }

    public String getDrExpertCode() {
        return drExpertCode;
    }

    public void setDrExpertCode(String drExpertCode) {
        this.drExpertCode = drExpertCode;
    }

    public String getDrExpertName() {
        return drExpertName;
    }

    public void setDrExpertName(String drExpertName) {
        this.drExpertName = drExpertName;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "drId=" + drId +
                ", drCode='" + drCode + '\'' +
                ", drName='" + drName + '\'' +
                ", drPinyin='" + drPinyin + '\'' +
                ", password='" + password + '\'' +
                ", drCardid='" + drCardid + '\'' +
                ", drDeptCode='" + drDeptCode + '\'' +
                ", drDeptName='" + drDeptName + '\'' +
                ", drLevelCode='" + drLevelCode + '\'' +
                ", drLevelName='" + drLevelName + '\'' +
                ", drPositionCode='" + drPositionCode + '\'' +
                ", drPositionName='" + drPositionName + '\'' +
                ", drPhoto='" + drPhoto + '\'' +
                ", drPhone='" + drPhone + '\'' +
                ", drEmail='" + drEmail + '\'' +
                ", drSex='" + drSex + '\'' +
                ", drBirthday=" + drBirthday +
                ", drEdu='" + drEdu + '\'' +
                ", drCountry='" + drCountry + '\'' +
                ", drNation='" + drNation + '\'' +
                ", drNativePlace='" + drNativePlace + '\'' +
                ", drState='" + drState + '\'' +
                ", drIntroduce='" + drIntroduce + '\'' +
                ", drGood='" + drGood + '\'' +
                ", drExpertCode='" + drExpertCode + '\'' +
                ", drExpertName='" + drExpertName + '\'' +
                '}';
    }
}
