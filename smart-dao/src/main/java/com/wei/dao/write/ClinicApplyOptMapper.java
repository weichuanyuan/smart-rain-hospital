package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.ClinicApply;

public interface ClinicApplyOptMapper extends IBaseOptDao<ClinicApply,Integer> {
}
