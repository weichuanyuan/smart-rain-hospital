package com.wei.entity.dto;

import java.math.BigDecimal;

public class ChargeDto {
    /**
     * 处方号
     */
    private Integer prescriptionId;
    /**
     * 费用名称
     */
    private String chargeName;
    /**
     * 费用数量
     */
    private BigDecimal chargeAmount;
    /**
     * 费用单价
     */
    private BigDecimal chargePrice;
    /**
     * 总金额
     */
    private BigDecimal chargeFee;
    /**
     * 规格
     */
    private String specifications;

    public Integer getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(Integer prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public String getChargeName() {
        return chargeName;
    }

    public void setChargeName(String chargeName) {
        this.chargeName = chargeName;
    }

    public BigDecimal getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(BigDecimal chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public BigDecimal getChargePrice() {
        return chargePrice;
    }

    public void setChargePrice(BigDecimal chargePrice) {
        this.chargePrice = chargePrice;
    }

    public BigDecimal getChargeFee() {
        return chargeFee;
    }

    public void setChargeFee(BigDecimal chargeFee) {
        this.chargeFee = chargeFee;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }
}
