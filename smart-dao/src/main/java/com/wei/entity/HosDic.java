package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class HosDic implements Serializable {

    /**
    * 主键
    */
    private Integer id;

    /**
    * 类型
    */
    private String type;

    /**
    * 编码
    */
    private String code;

    /**
    * 名称
    */
    private String name;

    /**
    * 上级编码
    */
    private String parentCode;

    /**
    * 拼音
    */
    private String py;

    /**
    * 备注
    */
    private String remark;

    /**
    * 修改时间
    */
    private Date modDt;

    /**
    * 修改人编码
    */
    private Integer modCode;


    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getParentCode() {
        return parentCode;
    }
    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getPy() {
        return py;
    }
    public void setPy(String py) {
        this.py = py;
    }

    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getModDt() {
        return modDt;
    }
    public void setModDt(Date modDt) {
        this.modDt = modDt;
    }

    public Integer getModCode() {
        return modCode;
    }
    public void setModCode(Integer modCode) {
        this.modCode = modCode;
    }


    @Override
    public String toString() {
        return "HosDic{" +
        "id='" + id + '\'' +
        "type='" + type + '\'' +
        "code='" + code + '\'' +
        "name='" + name + '\'' +
        "parentCode='" + parentCode + '\'' +
        "py='" + py + '\'' +
        "remark='" + remark + '\'' +
        "modDt='" + modDt + '\'' +
        "modCode='" + modCode + '\'' +
    '}';
    }

}
