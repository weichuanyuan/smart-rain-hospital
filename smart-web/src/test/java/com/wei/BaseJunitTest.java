package com.wei;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:/spring-mvc.xml",
        "classpath*:/applicationContext.xml",
        "classpath*:/applicationContext-mybatis.xml"})
public class BaseJunitTest {

    @Before
    public  void init(){

    }

    @After
    public  void after(){

    }
}
