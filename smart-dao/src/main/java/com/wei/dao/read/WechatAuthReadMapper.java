package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.WechatAuth;

public interface WechatAuthReadMapper extends IBaseReadDao<WechatAuth,String> {

}