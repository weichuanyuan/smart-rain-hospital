package com.wei.service.impl;

import com.wei.dao.Page;
import com.wei.dao.read.SchedulingReadMapper;
import com.wei.dao.write.SchedulingOptMapper;
import com.wei.entity.Scheduling;
import com.wei.entity.vo.SchedulingVo;
import com.wei.service.ISchedulingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SchedulingServiceImpl implements ISchedulingService {
    @Autowired
    private SchedulingOptMapper schedulingOptMapper;
    @Autowired
    private SchedulingReadMapper schedulingReadMapper;

    public boolean insert(Scheduling scheduling) {
        return schedulingOptMapper.insert(scheduling) > 0;
    }

    public boolean update(Scheduling scheduling) {
        return schedulingOptMapper.update(scheduling) > 0;
    }

    public boolean delete(Integer id) {
        return schedulingOptMapper.delete(id) > 0;
    }

    public boolean batchSava(List<Scheduling> schedulingList) {
        return schedulingOptMapper.batchSava(schedulingList) > 0;
    }

    public Scheduling getById(Integer id) {
        return schedulingReadMapper.getById(id);
    }

    public Integer totalCount() {
        return schedulingReadMapper.totalCount();
    }

    public List<Scheduling> getAll(Page<Scheduling> page) {
        return schedulingReadMapper.getAll(page);
    }

    public List<SchedulingVo> getDoctorSchedulingList(Map<String,String> maps){
        return schedulingReadMapper.getSchedulingByCondition(maps);
    }
}
