package com.wei.controller;

import com.wei.entity.dto.PatientDto;
import com.wei.service.IPatientService;
import com.wei.web.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/patient")
@Api(value = "/patientController", tags = "患者信息相关api")
public class PatientController {

    @Autowired
    private IPatientService patientService;

    @ResponseBody
    @PostMapping("/createPatient")
    @ApiOperation("新建患者")
    public ResultVo createPatient(@RequestBody PatientDto patient){
        return new ResultVo("200","生成成功",patientService.createPatient(patient));
    }
    @ResponseBody
    @PostMapping("/getBindPatientDtoList")
    @ApiOperation("根据token获取绑定的全部病人信息")
    public ResultVo getBindPatientDtoList(){
        //TODO 需要单独将sql的日志打印出来 不需要Debug 日志 否则控制台输出太多了
        return new ResultVo("200","生成成功",patientService.getBindPatientDtoList());
    }
    @ResponseBody
    @PostMapping("/bindPatient")
    @ApiOperation("绑卡")
    public ResultVo bindPatient(@RequestBody PatientDto patient){
        return new ResultVo("200","绑卡成功",patientService.bindPatient(patient));
    }
    @ResponseBody
    @PostMapping("/unBindPatient")
    @ApiOperation("解绑")
    public ResultVo unBindPatient(@RequestBody Map<String,String> map){
        return new ResultVo("200","解绑成功",patientService.unBindPatient(map.get("ioNo")));
    }
    @ResponseBody
    @PostMapping("/fitCrtPatient")
    @ApiOperation("设置当前就诊人")
    public ResultVo fitCrtPatient(@RequestBody Map<String,String> map){
        return new ResultVo("200","设置成功",patientService.fitCrtPatient(map.get("ioNo")));
    }
    @ResponseBody
    @PostMapping("/getIoNo")
    @ApiOperation("获取就诊卡号")
    public ResultVo getIoNo(@RequestBody Map<String,String> map){
        return new ResultVo("200","设置成功",patientService.getIoNo(map));
    }



}
