package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PtRelation implements Serializable {

    /**
    * 绑定人id
    */
    private Integer relationPtRelationId;

    /**
    * 绑定人卡号
    */
    private String relationCardRelationNo;

    /**
    * 病人id
    */
    private Integer relationPtId;

    /**
    * 默认当前患者Y N
    */
    private String relationCrtFlag;

    /**
    * 病人卡号
    */
    private String relationCardNo;

    /**
    * 更新时间
    */
    private Date relationModDt;

    /**
    * 使用标志 Y 再用 N 不用
    */
    private String relationFlag;


    public Integer getRelationPtRelationId() {
        return relationPtRelationId;
    }
    public void setRelationPtRelationId(Integer relationPtRelationId) {
        this.relationPtRelationId = relationPtRelationId;
    }

    public String getRelationCardRelationNo() {
        return relationCardRelationNo;
    }
    public void setRelationCardRelationNo(String relationCardRelationNo) {
        this.relationCardRelationNo = relationCardRelationNo;
    }

    public Integer getRelationPtId() {
        return relationPtId;
    }
    public void setRelationPtId(Integer relationPtId) {
        this.relationPtId = relationPtId;
    }

    public String getRelationCrtFlag() {
        return relationCrtFlag;
    }
    public void setRelationCrtFlag(String relationCrtFlag) {
        this.relationCrtFlag = relationCrtFlag;
    }

    public String getRelationCardNo() {
        return relationCardNo;
    }
    public void setRelationCardNo(String relationCardNo) {
        this.relationCardNo = relationCardNo;
    }

    public Date getRelationModDt() {
        return relationModDt;
    }
    public void setRelationModDt(Date relationModDt) {
        this.relationModDt = relationModDt;
    }

    public String getRelationFlag() {
        return relationFlag;
    }
    public void setRelationFlag(String relationFlag) {
        this.relationFlag = relationFlag;
    }


    @Override
    public String toString() {
        return "PtRelation{" +
        "relationPtRelationId='" + relationPtRelationId + '\'' +
        "relationCardRelationNo='" + relationCardRelationNo + '\'' +
        "relationPtId='" + relationPtId + '\'' +
        "relationCrtFlag='" + relationCrtFlag + '\'' +
        "relationCardNo='" + relationCardNo + '\'' +
        "relationModDt='" + relationModDt + '\'' +
        "relationFlag='" + relationFlag + '\'' +
    '}';
    }

}
