package com.wei.service.impl;

import com.wei.dao.Page;
import com.wei.dao.read.DeptReadMapper;
import com.wei.dao.write.DeptOptMapper;
import com.wei.entity.Dept;
import com.wei.entity.dto.DeptDto;
import com.wei.service.IDeptService;
import com.wei.service.mapstruct.DeptMapper;
import com.wei.service.wrapper.HttpContextWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DeptServiceImpl implements IDeptService {
    @Autowired
    private DeptOptMapper deptOptMapper;
    @Autowired
    private DeptReadMapper deptReadMapper;
    @Autowired
    private HttpContextWrapper wrapper;

    public boolean insert(Dept dept) {
        return deptOptMapper.insert(dept) > 0;
    }

    public boolean update(Dept dept) {
        return deptOptMapper.update(dept) > 0;
    }

    public boolean delete(Integer id) {
        return deptOptMapper.delete(id) > 0;
    }

    public boolean batchSava(List<Dept> deptList) {
        return deptOptMapper.batchSava(deptList) > 0;
    }

    public Dept getById(Integer id) {
        return deptReadMapper.getById(id);
    }

    public Integer totalCount() {
        return deptReadMapper.totalCount();
    }

    public List<Dept> getAll(Page<Dept> page) {
        return deptReadMapper.getAll(page);
    }

    public  List<DeptDto> getFamousDeptList(){
        return DeptMapper.INSTANCE.deptListToDeptDtoList( deptReadMapper.getFamousDeptList());
    }
}
