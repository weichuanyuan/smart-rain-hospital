package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.WechatAuth;

import java.util.List;

public interface IWechatAuthService {

    boolean insert(WechatAuth wechatAuth);

    boolean update(WechatAuth wechatAuth);

    boolean delete(Integer id);

    boolean batchSava(List<WechatAuth> wechatAuthList);

    List<WechatAuth> getAll(Page<WechatAuth> page);

    Integer totalCount();

    WechatAuth getById(String id);

    String getToken(String code, String appid, String secret);

    boolean sendVerifyCode(String phoneNumber);

    boolean verifyCode(String phoneNumber,String verifyCode);
}