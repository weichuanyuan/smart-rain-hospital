package com.wei.dao;

import java.io.Serializable;

public class Sort implements Serializable {

    private static final long serialVersionUID = 4399191467934985131L;

    private String field;
    private  Direction direction;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Sort{" +
                "field='" + field + '\'' +
                ", direction=" + direction +
                '}';
    }
}
