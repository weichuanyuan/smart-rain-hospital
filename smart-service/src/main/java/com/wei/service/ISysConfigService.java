package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.SysConfig;

import java.util.List;

public interface ISysConfigService {

    boolean insert(SysConfig sysConfig);

    boolean update(SysConfig sysConfig);

    boolean delete(Integer id);

    boolean batchSava(List<SysConfig> sysConfigList);

    SysConfig getById(Integer id);

    List<SysConfig> getAll(Page<SysConfig> page);

    Integer totalCount();
}