package com.wei.dao.write;

import com.wei.dao.IBaseOptDao;
import com.wei.entity.HosDic;

public interface HosDicOptMapper extends IBaseOptDao<HosDic,Integer> {
}
