package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.Prescription;
import com.wei.entity.dto.ChargeResultDto;
import com.wei.entity.dto.MainDto;
import com.wei.entity.dto.PrescriptionDto;

import java.util.List;
import java.util.Map;

public interface IPrescriptionService {

    boolean insert(Prescription prescription);

    boolean update(Prescription prescription);

    boolean delete(Integer id);

    boolean batchSava(List<Prescription> prescriptionList);

    Prescription getById(Integer id);

    List<Prescription> getAll(Page<Prescription> page);

    Integer totalCount();

    List<MainDto> getPrescriptionDtoList(Map<String,String> map);

    ChargeResultDto charge(Map<String, String> map);

}