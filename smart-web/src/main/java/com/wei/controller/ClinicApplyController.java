package com.wei.controller;

import com.wei.service.IClinicApplyService;
import com.wei.web.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/clinicApply")
@Api(value = "/clinicApplyController", tags = "检查检验相关api")
public class ClinicApplyController {
    @Autowired
    private IClinicApplyService clinicApplyService;


    @ResponseBody
    @PostMapping("/getApply")
    @ApiOperation("获取检查检验申请单")
    public ResultVo getApply(@RequestBody Map<String,String> map){
        return new ResultVo("200","查询成功",clinicApplyService.getApply(map));
    }

    @ResponseBody
    @PostMapping("/getApplyResult")
    @ApiOperation("获取检查检验结果")
    public ResultVo getApplyResult(@RequestBody String applyNo){
        return new ResultVo("200","查询成功",clinicApplyService.getApplyResult(applyNo));
    }
}
