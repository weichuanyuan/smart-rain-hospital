package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.SchedulingDetail;
import com.wei.entity.dto.AppointRecordDto;

import java.util.List;
import java.util.Map;

public interface ISchedulingDetailService {

    boolean insert(SchedulingDetail schedulingDetail);

    boolean update(SchedulingDetail schedulingDetail);

    boolean delete(Integer id);

    boolean batchSava(List<SchedulingDetail> schedulingDetailList);

    SchedulingDetail getById(Integer id);

    List<SchedulingDetail> getAll(Page<SchedulingDetail> page);

    Integer totalCount();
    List<SchedulingDetail> getRegistrationSource(Map<String,String> maps);
    boolean appointment(Map<String, String> maps);
    boolean register(Map<String, String> maps);
    boolean cancelAppointment(Map<String, String> maps);
    List<AppointRecordDto> getAppointRecord(Map<String, String> maps);
    List<AppointRecordDto> getRegisterRecord(Map<String, String> maps);
    boolean updateBreakAppoint();
}