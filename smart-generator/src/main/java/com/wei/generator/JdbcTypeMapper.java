package com.wei.generator;

public class JdbcTypeMapper {
    public JdbcTypeMapper(Class<?> javaType,String jdbcType){
        this.javaType = javaType;
        this.jdbcType = jdbcType;
    }

    private Class<?> javaType;
    private String jdbcType;

    public Class<?> getJavaType() {
        return javaType;
    }

    public void setJavaType(Class<?> javaType) {
        this.javaType = javaType;
    }

    public String getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(String jdbcType) {
        this.jdbcType = jdbcType;
    }
}
