package com.wei.entity.vo;

import com.wei.entity.Charge;
import com.wei.entity.Prescription;

import java.util.List;

public class PrescriptionVo extends Prescription {
    /**
     * 开单科室
     */
    private String prescriptionDeptName;
    /**
     * 执行科室
     */
    private String execDeptName;
    /**
     * 开单医生
     */
    private String prescriptionDrname;

    /**
     * 处方类型
     */
    private String prescriptionTypeName;
    /**
     * 费用信息
     */
    private List<Charge> chargeList;

    public List<Charge> getChargeList() {
        return chargeList;
    }

    public void setChargeList(List<Charge> chargeList) {
        this.chargeList = chargeList;
    }

    private String guideDeptAddr;

    public String getExecDeptName() {
        return execDeptName;
    }

    public void setExecDeptName(String execDeptName) {
        this.execDeptName = execDeptName;
    }

    public String getGuideDeptAddr() {
        return guideDeptAddr;
    }

    public void setGuideDeptAddr(String guideDeptAddr) {
        this.guideDeptAddr = guideDeptAddr;
    }

    public String getPrescriptionTypeName() {
        return prescriptionTypeName;
    }

    public void setPrescriptionTypeName(String prescriptionTypeName) {
        this.prescriptionTypeName = prescriptionTypeName;
    }

    public String getPrescriptionDeptName() {
        return prescriptionDeptName;
    }

    public void setPrescriptionDeptName(String prescriptionDeptName) {
        this.prescriptionDeptName = prescriptionDeptName;
    }

    public String getPrescriptionDrname() {
        return prescriptionDrname;
    }

    public void setPrescriptionDrname(String prescriptionDrname) {
        this.prescriptionDrname = prescriptionDrname;
    }
}
