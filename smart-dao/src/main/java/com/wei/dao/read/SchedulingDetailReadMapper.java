package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.Appointment;
import com.wei.entity.SchedulingDetail;
import com.wei.entity.vo.AppointmentVo;
import com.wei.entity.vo.SchedulingDetailVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SchedulingDetailReadMapper extends IBaseReadDao<SchedulingDetail,Integer> {
     List<SchedulingDetail> getRegistrationSource(Map<String,String> maps);

     SchedulingDetailVo getSchedulingDetailVoBySourceId(@Param("sourceId") String sourceId);

     Integer getAppointmentCheck(@Param("scheId")String scheId,
                                 @Param("ptId")String ptId,
                                 @Param("apn")String apn);

     Integer getRegisterCheck(@Param("scheId")String scheId,
                              @Param("ptId")String ptId,
                              @Param("apn")String apn);

     Integer getBreachAppointment(@Param("ptId")String ptId);

     List<AppointmentVo> getAppointmentVoByMap(Map<String,String> maps);

     List<AppointmentVo> getAppointmentVoByRegisterMap(Map<String,String> maps);

     List<Appointment> getBreakAppointmentList();
}