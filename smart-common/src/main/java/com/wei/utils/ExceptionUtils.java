package com.wei.utils;


import com.wei.utils.exception.*;
public final class ExceptionUtils {
    private ExceptionUtils() {
    }

    /**
     * 返回一个新的异常，统一构建，方便统一处理
     *
     * @param msg 消息
     * @param t   异常信息
     * @return 返回异常
     */
    public static WorkingException mpe(String msg, Throwable t, Object... params) {
        return  new WorkingException(Stringutils.format(msg, params), t);
    }

    /**
     * 重载的方法
     *
     * @param msg 消息
     * @return 返回异常
     */
    public static WorkingException mpe(String msg, Object... params) {
        return new WorkingException(Stringutils.format(msg, params));
    }

    /**
     * 重载的方法
     *
     * @param t 异常
     * @return 返回异常
     */
    public static WorkingException mpe(Throwable t) {
        return new WorkingException(t);
    }

    public static ValidatorException mpeValidator(String msg, Throwable t, Object... params)  {
        return new ValidatorException(Stringutils.format(msg, params), t);
    }

    public static ValidatorException mpeValidator(String msg, Object... params){
        return new ValidatorException(Stringutils.format(msg, params));
    }

    public static ValidatorException mpeValidator(Throwable t){
        return new ValidatorException(t);
    }
}
