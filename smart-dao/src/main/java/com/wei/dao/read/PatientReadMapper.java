package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.Patient;
import com.wei.entity.vo.PatientVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PatientReadMapper extends IBaseReadDao<Patient,Integer> {
    Patient getByCardno(@Param("cardno") String cardno);

    Patient getByIoNo(@Param("ioNo") String ioNo);

    List<PatientVo> getBindPatientVoList(String token);

    Patient getPatientByToken(String token);

    Patient getByGuardCardno(@Param("cardno") String cardno);

    Integer getMaxVisitSeries(@Param("ptId") Integer ptId);
}