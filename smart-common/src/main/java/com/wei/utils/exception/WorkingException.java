package com.wei.utils.exception;

public class WorkingException extends RuntimeException {
    private static final long serialVersionUID = -2403418560831734128L;

    public WorkingException(String message){
        super(message);
    }

    public WorkingException(Throwable throwable){
        super(throwable);
    }

    public WorkingException(String message,Throwable throwable){
        super(message,throwable);
    }
}
