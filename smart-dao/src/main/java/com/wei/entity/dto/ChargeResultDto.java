package com.wei.entity.dto;

public class ChargeResultDto {
    /**
     * 流水ID
     */
    private String flowId;
    /**
     * 窗口指引
     */
    private String window;
    /**
     * 科室指引
     */
    private String guide;
    /**
     * 总金额
     */
    private String fee;

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }
}
