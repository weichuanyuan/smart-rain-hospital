package com.wei.entity.vo;

import com.wei.entity.Scheduling;

import java.math.BigDecimal;

public class SchedulingVo extends Scheduling {

    private String drName;
    private String drLevelName;
    private String deptName;
    private String drPinyin;
    private String deptPinyin;
    private String drPhoto;
    private String introduce;
    /**
     * 挂号费
     */
    private BigDecimal fee;
    /**
     * 小孩挂号费
     */
    private BigDecimal childFee;

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getChildFee() {
        return childFee;
    }

    public void setChildFee(BigDecimal childFee) {
        this.childFee = childFee;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getDrPhoto() {
        return drPhoto;
    }

    public void setDrPhoto(String drPhoto) {
        this.drPhoto = drPhoto;
    }

    public String getDrName() {
        return drName;
    }

    public void setDrName(String drName) {
        this.drName = drName;
    }

    public String getDrLevelName() {
        return drLevelName;
    }

    public void setDrLevelName(String drLevelName) {
        this.drLevelName = drLevelName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDrPinyin() {
        return drPinyin;
    }

    public void setDrPinyin(String drPinyin) {
        this.drPinyin = drPinyin;
    }

    public String getDeptPinyin() {
        return deptPinyin;
    }

    public void setDeptPinyin(String deptPinyin) {
        this.deptPinyin = deptPinyin;
    }
}
