package com.wei.entity.vo;

import com.wei.entity.SchedulingDetail;

import java.util.Date;

public class SchedulingDetailVo extends SchedulingDetail {

    /**
     * 上午使用状态 Y 再用 N 停用
     */
    private String scheAmState;

    /**
     * 下午使用状态 Y 再用 N 停用
     */
    private String schePmState;

    /**
     * 晚上使用状态 Y 再用 N 停用
     */
    private String scheEveState;

    /**
     * 排班时间
     */
    private Date scheDate;
    /**
     * 挂号类型
     */
    private String scheRegTypeCode;

    /**
     * 挂号类型名称
     */
    private String scheRegTypeName;

    private String drCode;
    private String deptCode;
    private String deptName;
    private String deptPinyin;

    public String getDeptPinyin() {
        return deptPinyin;
    }

    public void setDeptPinyin(String deptPinyin) {
        this.deptPinyin = deptPinyin;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDrCode() {
        return drCode;
    }

    public void setDrCode(String drCode) {
        this.drCode = drCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getScheRegTypeCode() {
        return scheRegTypeCode;
    }

    public void setScheRegTypeCode(String scheRegTypeCode) {
        this.scheRegTypeCode = scheRegTypeCode;
    }

    public String getScheRegTypeName() {
        return scheRegTypeName;
    }

    public void setScheRegTypeName(String scheRegTypeName) {
        this.scheRegTypeName = scheRegTypeName;
    }

    public Date getScheDate() {
        return scheDate;
    }

    public void setScheDate(Date scheDate) {
        this.scheDate = scheDate;
    }

    public String getScheAmState() {
        return scheAmState;
    }

    public void setScheAmState(String scheAmState) {
        this.scheAmState = scheAmState;
    }

    public String getSchePmState() {
        return schePmState;
    }

    public void setSchePmState(String schePmState) {
        this.schePmState = schePmState;
    }

    public String getScheEveState() {
        return scheEveState;
    }

    public void setScheEveState(String scheEveState) {
        this.scheEveState = scheEveState;
    }
}
