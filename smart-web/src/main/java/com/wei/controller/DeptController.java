package com.wei.controller;

import com.wei.dao.Page;
import com.wei.entity.Dept;
import com.wei.service.IDeptService;
import com.wei.web.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/dept")
@Api(value = "/deptController", tags = "科室")

public class DeptController {
    @Autowired
    private IDeptService deptService;

    @ResponseBody
    @PostMapping("/getAll")
    @ApiOperation("根据条件获取List<Dept>")
    public ResultVo getAll(@RequestBody Page<Dept> page) {
        return new ResultVo("200", "查询成功", deptService.getAll(page));
    }

    @ResponseBody
    @PostMapping("/getFamousDeptList")
    @ApiOperation("获取医院知名科室列表")
    public ResultVo getFamousDeptList() {
        return new ResultVo("200", "查询成功", deptService.getFamousDeptList());
    }
}
