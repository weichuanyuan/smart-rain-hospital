package com.wei.controller;


import com.wei.annotation.RepeatVerify;
import com.wei.service.IEmployeeService;
import com.wei.web.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/doctor")
@Api(value = "/doctorController", tags = "医生")
public class DoctorController {
    @Autowired
    private IEmployeeService doctorService;

    @ResponseBody
    @PostMapping("/getDoctorDtoList")
    @ApiOperation("获取所有正常状态的医生列表")
    public ResultVo getDoctorDtoList(@RequestBody String features){
        return new ResultVo("200","查询成功",doctorService.getDoctorDtoList(features));
    }
    @ResponseBody
    @PostMapping("/getExpertDoctor")
    @ApiOperation("获取所有专家")

    public  ResultVo getExpertDoctor(){
        return new ResultVo("200","查询成功",doctorService.getExpertDoctor());
    }
}
