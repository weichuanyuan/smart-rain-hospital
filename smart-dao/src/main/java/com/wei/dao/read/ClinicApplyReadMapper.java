package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.ClinicApply;
import com.wei.entity.ClinicResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ClinicApplyReadMapper extends IBaseReadDao<ClinicApply,Integer> {
    List<ClinicApply> getApply(Map<String, String> map);
    List<ClinicResult> getApplyResult(@Param("applyNo") String applyNo);
}