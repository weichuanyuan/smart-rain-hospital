package com.wei.service.mapstruct;

import com.wei.entity.dto.AppointRecordDto;
import com.wei.entity.vo.AppointmentVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AppointmentMapper {

    AppointmentMapper INSTANCE = Mappers.getMapper(AppointmentMapper.class);

    @Mappings({})
    AppointRecordDto AppointmentVoToAppointRecordDto(AppointmentVo appointmentVo);
    List<AppointRecordDto> AppointmentVoListToAppointRecordDtoList(List<AppointmentVo> appointmentVoList);
}
