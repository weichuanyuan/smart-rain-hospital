package com.wei.dao;

import java.io.Serializable;
import java.util.List;

public interface IBaseReadDao<T extends Serializable,PK> {

    /**
     * 根据id获取实体对象
     * @param id
     * @return
     */
    T getById(PK id);

    /**
     * 据Page获取满足条件的实体对象
     * @param page
     * @return
     */
    List<T> getAll(Page<T> page);

    /**
     * 获取总记录数
     * @return
     */
    Integer totalCount();
}
