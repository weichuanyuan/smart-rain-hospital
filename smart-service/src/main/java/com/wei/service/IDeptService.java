package com.wei.service;

import com.wei.dao.Page;
import com.wei.entity.Dept;
import com.wei.entity.dto.DeptDto;

import java.util.List;

public interface IDeptService {

    boolean insert(Dept dept);

    boolean update(Dept dept);

    boolean delete(Integer id);

    boolean batchSava(List<Dept> deptList);

    Dept getById(Integer id);

    List<Dept> getAll(Page<Dept> page);

    Integer totalCount();

    List<DeptDto> getFamousDeptList();
}