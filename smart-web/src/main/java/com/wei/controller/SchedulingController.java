package com.wei.controller;


import com.wei.annotation.AuthorityVerify;
import com.wei.annotation.RepeatVerify;
import com.wei.service.ISchedulingDetailService;
import com.wei.service.ISchedulingService;
import com.wei.web.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/scheduling")
@Api(value = "/schedulingController", tags = "排班管理")
@AuthorityVerify
public class SchedulingController {
    @Autowired
    private ISchedulingService schedulingService;

    @Autowired
    private ISchedulingDetailService schedulingDetailService;

    @ResponseBody
    @PostMapping("/getDoctorSchedulingList")
    @ApiOperation("根据条件获取医生排班")
    public ResultVo getDoctorSchedulingList(@RequestBody Map<String, String> maps) {
        return new ResultVo("200", "查询成功!", schedulingService.getDoctorSchedulingList(maps));
    }

    @ResponseBody
    @PostMapping("/getRegistrationSource")
    @ApiOperation("根据条件获取医生排班号源")
    public ResultVo getRegistrationSource(@RequestBody Map<String, String> maps) {
        return new ResultVo("200", "查询成功!", schedulingDetailService.getRegistrationSource(maps));
    }
    @ResponseBody
    @PostMapping("/appointment")
    @ApiOperation("预约号源")
    @RepeatVerify(primary ={"sourceId","ioNo"})
    public ResultVo appointment(@RequestBody Map<String,String> maps){
        return new ResultVo("200","预约成功!",schedulingDetailService.appointment(maps));
    }

    @ResponseBody
    @PostMapping("/register")
    @ApiOperation("当天挂号")
    @RepeatVerify(primary = {"sourceId","ioNo"})
    public ResultVo register(@RequestBody Map<String,String> maps){
        return new ResultVo("200","挂号成功!",schedulingDetailService.register(maps));
    }
    @ResponseBody
    @PostMapping("/cancelAppointment")
    @ApiOperation("取消预约")
    public ResultVo cancelAppointment(@RequestBody Map<String,String> maps){
        return new ResultVo("200","取消预约成功!",schedulingDetailService.cancelAppointment(maps));
    }

    @ResponseBody
    @PostMapping("/getAppointRecord")
    @ApiOperation("获取预约记录")
    public ResultVo getAppointRecord(@RequestBody Map<String,String> maps){
        return new ResultVo("200","获取预约记录!",schedulingDetailService.getAppointRecord(maps));
    }

    @ResponseBody
    @PostMapping("/getRegisterRecord")
    @ApiOperation("获取挂号记录")
    public ResultVo getRegisterRecord(@RequestBody Map<String,String> maps){
        return new ResultVo("200","获取挂号记录!",schedulingDetailService.getRegisterRecord(maps));
    }
}
