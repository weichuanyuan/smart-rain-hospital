package com.wei.utils;

import java.util.ArrayList;
import java.util.List;

public final class ApplicationConstants {
    private ApplicationConstants(){}
    public static final Class<?>[] PRIMITIVE_TYPES = { int.class, long.class, short.class,
            float.class, double.class, byte.class, boolean.class, char.class, Integer.class, Long.class,
            Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class };

    public static final String TOKEN = "token";
    public static  final String COMMA = ",";
    public static  final String SPACE = " ";
    public static  final String TRUE = "true";
    public static  final String FALSE = "false";
    public static  final String YES = "yes";
    public static  final String NO = "no";
    public static  final String EMPTY = "";
    public static  final String DESC = "desc";
    public static  final String ASC = "asc";
    public static  final String UNDERLINE = "_";
    public static  final String SingleQuotation="'";
    public static final String ORDER_BY = "order by";
    public static final String WHERE = "where";
    public static final String EQUAL = "1=1";
    public static final String SLASH = "/";
    public static final String OK = "Ok";
    public static final String UTF8 = "UTF-8";
    public static final String Y = "Y";
    public static final String N = "N";
    public static final String POST = "POST";
    public static final String GET = "GET";
    /**
     * 主键
     */
    public static final String PRI="PRI";
    /**
     * 唯一索引
     */
    public static final String UNI="UNI";
    /**
     * 一般索引
     */
    public static final String MUL="MUL";
}
