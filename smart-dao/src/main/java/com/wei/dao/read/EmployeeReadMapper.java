package com.wei.dao.read;


import com.wei.dao.IBaseReadDao;
import com.wei.entity.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeReadMapper extends IBaseReadDao<Employee,Integer> {
    /**
     * 获取所有的门诊医生
     * @return
     */
    List<Employee> getDoctorDtoList(@Param("features") String features);

    /**
     * 获取专家
     * @return
     */
    List<Employee> getExpertDoctor();
}