package com.wei.dao;

import com.wei.utils.ApplicationConstants;
import com.wei.common.DbColGenerater;
import com.wei.utils.CollectionUtils;

import java.io.Serializable;
import java.util.List;

/**
 * 为什么java必须设置成抽象类才能获取到泛型T
 *
 * @param <T>
 */
public abstract class QueryWrapper<T extends Serializable> implements Serializable {

    /**
     * 查询条件
     */
    private List<QueryStandard> queryStandardList;
    /**
     * 排序条件
     */
    private List<Sort> sortList;

    /**
     * 组织的条件
     *
     * @return
     */
    public String getQueryCondition() {
//        Class<T> clazz =(Class<T>)ReflectionsUtils.getSuperClassGenericType(getClass(),0);
//        Map<String, Field> fields = ReflectionsUtils.getFieldMap(clazz);

        if (CollectionUtils.isEmpty(queryStandardList)) {
            return ApplicationConstants.EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        // where 1=1
        sb.append(ApplicationConstants.SPACE).append(ApplicationConstants.WHERE)
                .append(ApplicationConstants.SPACE);

        Integer index = 0;
        for (QueryStandard query : queryStandardList) {
            if(index>0){
                sb.append(ApplicationConstants.SPACE);
                if (query.getRelation() == null) {
                    sb.append(QueryRelation.AND.getValue());
                } else {
                    sb.append(query.getRelation().getValue());
                }
                sb.append(ApplicationConstants.SPACE);
            }
            index++;
            sb.append(DbColGenerater.generaterDbColName(query.getField()))
              .append(query.getCondition().getValue())
              .append(ApplicationConstants.SingleQuotation).append(query.getValue())
              .append(ApplicationConstants.SingleQuotation);
        }

        return sb.toString();
    }

    /**
     * 组织的排序条件
     *
     * @return
     */
    public String getQuerySort() {
        if (CollectionUtils.isEmpty(sortList)) {
            return ApplicationConstants.EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(ApplicationConstants.SPACE).append(ApplicationConstants.ORDER_BY).append(ApplicationConstants.SPACE);
        int index = 0;
        for (Sort sort : sortList) {
            index++;
            sb.append(DbColGenerater.generaterDbColName(sort.getField()));
            sb.append(ApplicationConstants.SPACE);
            sb.append(sort.getDirection().getValue());
            if (index < sortList.size()) {
                sb.append(ApplicationConstants.COMMA);
            }
        }
        return sb.toString();
    }

    public List<Sort> getSortList() {
        return sortList;
    }

    public void setSortList(List<Sort> sortList) {
        this.sortList = sortList;
    }

    public List<QueryStandard> getQueryStandardList() {
        return queryStandardList;
    }

    public void setQueryStandardList(List<QueryStandard> queryStandardList) {
        this.queryStandardList = queryStandardList;
    }
}
