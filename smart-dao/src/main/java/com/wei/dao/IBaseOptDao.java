package com.wei.dao;

import java.io.Serializable;
import java.util.List;

public interface IBaseOptDao<T extends Serializable,PK> {
    Integer insert(T usr);

    int update(T usr);

    int batchSava(List<T> usrList);

    int delete(PK usrId);
}
