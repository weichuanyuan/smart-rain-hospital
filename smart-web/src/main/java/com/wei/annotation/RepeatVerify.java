package com.wei.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatVerify {
    /**
     * 主键字段
     * @return
     */
    String[] primary() default "";

    /**
     * 重复请求超时时间
     * @return
     */
    long timeout() default 3000;
}
