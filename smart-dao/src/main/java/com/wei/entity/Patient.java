package com.wei.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Patient implements Serializable {

    /**
    * 主键id
    */
    private Integer ptId;

    /**
    * 门诊/住院号
    */
    private String ptIoNo;

    /**
    * 病案号
    */
    private String ptMedicalNo;

    /**
    * 姓名
    */
    private String ptName;

    /**
    * 身份证号
    */
    private String ptCardid;

    /**
    * 01 身份证 02 护照 03 香港身份证 04 澳门身份证 05 台湾身份证
    */
    private String ptCardtype;

    /**
    * 年龄
    */
    private String ptAge;

    /**
    * 生日
    */
    private Date ptBirthday;

    /**
    * 拼音
    */
    private String ptPinyin;

    /**
    * 社保号
    */
    private String ptInsuNo;

    /**
    * 电话号码
    */
    private String ptPhone;

    /**
    * 性别
    */
    private String ptSex;

    /**
    * 学历
    */
    private String ptEdu;

    /**
    * 国家
    */
    private String ptCountry;

    /**
    * 民族
    */
    private String ptNation;

    /**
    * 省编码
    */
    private String ptProvinceCode;

    /**
    * 省名称
    */
    private String ptProvinceName;

    /**
    * 市编码
    */
    private String ptCityCode;

    /**
    * 市名称
    */
    private String ptCityName;

    /**
    * 县名称
    */
    private String ptCountyCode;

    /**
    * 县名称
    */
    private String ptCountyName;

    /**
    * 具体地址
    */
    private String ptTown;

    /**
    * 地址
    */
    private String ptAddress;

    /**
    * 联系人姓名
    */
    private String ptContacts;

    /**
    * 与联系人关系
    */
    private String ptContactsRelation;

    /**
    * 联系人电话
    */
    private String ptContactsPhone;

    /**
    * 邮政编码
    */
    private String ptPostalCode;

    /**
    * 建档人编码
    */
    private String ptCreateCode;

    /**
    * 建档时间
    */
    private Date ptCreateDt;

    /**
    * 是否是小孩Y/N
    */
    private String ptChild;

    /**
    * 监护人卡号类型 01 身份证 02 护照 03 香港身份证 04 澳门身份证 05 台湾身份证
    */
    private String ptGuardianCardtype;

    /**
    * 监护人卡号
    */
    private String ptGuardianCardno;


    public Integer getPtId() {
        return ptId;
    }
    public void setPtId(Integer ptId) {
        this.ptId = ptId;
    }

    public String getPtIoNo() {
        return ptIoNo;
    }
    public void setPtIoNo(String ptIoNo) {
        this.ptIoNo = ptIoNo;
    }

    public String getPtMedicalNo() {
        return ptMedicalNo;
    }
    public void setPtMedicalNo(String ptMedicalNo) {
        this.ptMedicalNo = ptMedicalNo;
    }

    public String getPtName() {
        return ptName;
    }
    public void setPtName(String ptName) {
        this.ptName = ptName;
    }

    public String getPtCardid() {
        return ptCardid;
    }
    public void setPtCardid(String ptCardid) {
        this.ptCardid = ptCardid;
    }

    public String getPtCardtype() {
        return ptCardtype;
    }
    public void setPtCardtype(String ptCardtype) {
        this.ptCardtype = ptCardtype;
    }

    public String getPtAge() {
        return ptAge;
    }
    public void setPtAge(String ptAge) {
        this.ptAge = ptAge;
    }

    public Date getPtBirthday() {
        return ptBirthday;
    }
    public void setPtBirthday(Date ptBirthday) {
        this.ptBirthday = ptBirthday;
    }

    public String getPtPinyin() {
        return ptPinyin;
    }
    public void setPtPinyin(String ptPinyin) {
        this.ptPinyin = ptPinyin;
    }

    public String getPtInsuNo() {
        return ptInsuNo;
    }
    public void setPtInsuNo(String ptInsuNo) {
        this.ptInsuNo = ptInsuNo;
    }

    public String getPtPhone() {
        return ptPhone;
    }
    public void setPtPhone(String ptPhone) {
        this.ptPhone = ptPhone;
    }

    public String getPtSex() {
        return ptSex;
    }
    public void setPtSex(String ptSex) {
        this.ptSex = ptSex;
    }

    public String getPtEdu() {
        return ptEdu;
    }
    public void setPtEdu(String ptEdu) {
        this.ptEdu = ptEdu;
    }

    public String getPtCountry() {
        return ptCountry;
    }
    public void setPtCountry(String ptCountry) {
        this.ptCountry = ptCountry;
    }

    public String getPtNation() {
        return ptNation;
    }
    public void setPtNation(String ptNation) {
        this.ptNation = ptNation;
    }

    public String getPtProvinceCode() {
        return ptProvinceCode;
    }
    public void setPtProvinceCode(String ptProvinceCode) {
        this.ptProvinceCode = ptProvinceCode;
    }

    public String getPtProvinceName() {
        return ptProvinceName;
    }
    public void setPtProvinceName(String ptProvinceName) {
        this.ptProvinceName = ptProvinceName;
    }

    public String getPtCityCode() {
        return ptCityCode;
    }
    public void setPtCityCode(String ptCityCode) {
        this.ptCityCode = ptCityCode;
    }

    public String getPtCityName() {
        return ptCityName;
    }
    public void setPtCityName(String ptCityName) {
        this.ptCityName = ptCityName;
    }

    public String getPtCountyCode() {
        return ptCountyCode;
    }
    public void setPtCountyCode(String ptCountyCode) {
        this.ptCountyCode = ptCountyCode;
    }

    public String getPtCountyName() {
        return ptCountyName;
    }
    public void setPtCountyName(String ptCountyName) {
        this.ptCountyName = ptCountyName;
    }

    public String getPtTown() {
        return ptTown;
    }
    public void setPtTown(String ptTown) {
        this.ptTown = ptTown;
    }

    public String getPtAddress() {
        return ptAddress;
    }
    public void setPtAddress(String ptAddress) {
        this.ptAddress = ptAddress;
    }

    public String getPtContacts() {
        return ptContacts;
    }
    public void setPtContacts(String ptContacts) {
        this.ptContacts = ptContacts;
    }

    public String getPtContactsRelation() {
        return ptContactsRelation;
    }
    public void setPtContactsRelation(String ptContactsRelation) {
        this.ptContactsRelation = ptContactsRelation;
    }

    public String getPtContactsPhone() {
        return ptContactsPhone;
    }
    public void setPtContactsPhone(String ptContactsPhone) {
        this.ptContactsPhone = ptContactsPhone;
    }

    public String getPtPostalCode() {
        return ptPostalCode;
    }
    public void setPtPostalCode(String ptPostalCode) {
        this.ptPostalCode = ptPostalCode;
    }

    public String getPtCreateCode() {
        return ptCreateCode;
    }
    public void setPtCreateCode(String ptCreateCode) {
        this.ptCreateCode = ptCreateCode;
    }

    public Date getPtCreateDt() {
        return ptCreateDt;
    }
    public void setPtCreateDt(Date ptCreateDt) {
        this.ptCreateDt = ptCreateDt;
    }

    public String getPtChild() {
        return ptChild;
    }
    public void setPtChild(String ptChild) {
        this.ptChild = ptChild;
    }

    public String getPtGuardianCardtype() {
        return ptGuardianCardtype;
    }
    public void setPtGuardianCardtype(String ptGuardianCardtype) {
        this.ptGuardianCardtype = ptGuardianCardtype;
    }

    public String getPtGuardianCardno() {
        return ptGuardianCardno;
    }
    public void setPtGuardianCardno(String ptGuardianCardno) {
        this.ptGuardianCardno = ptGuardianCardno;
    }


    @Override
    public String toString() {
        return "Patient{" +
        "ptId='" + ptId + '\'' +
        "ptIoNo='" + ptIoNo + '\'' +
        "ptMedicalNo='" + ptMedicalNo + '\'' +
        "ptName='" + ptName + '\'' +
        "ptCardid='" + ptCardid + '\'' +
        "ptCardtype='" + ptCardtype + '\'' +
        "ptAge='" + ptAge + '\'' +
        "ptBirthday='" + ptBirthday + '\'' +
        "ptPinyin='" + ptPinyin + '\'' +
        "ptInsuNo='" + ptInsuNo + '\'' +
        "ptPhone='" + ptPhone + '\'' +
        "ptSex='" + ptSex + '\'' +
        "ptEdu='" + ptEdu + '\'' +
        "ptCountry='" + ptCountry + '\'' +
        "ptNation='" + ptNation + '\'' +
        "ptProvinceCode='" + ptProvinceCode + '\'' +
        "ptProvinceName='" + ptProvinceName + '\'' +
        "ptCityCode='" + ptCityCode + '\'' +
        "ptCityName='" + ptCityName + '\'' +
        "ptCountyCode='" + ptCountyCode + '\'' +
        "ptCountyName='" + ptCountyName + '\'' +
        "ptTown='" + ptTown + '\'' +
        "ptAddress='" + ptAddress + '\'' +
        "ptContacts='" + ptContacts + '\'' +
        "ptContactsRelation='" + ptContactsRelation + '\'' +
        "ptContactsPhone='" + ptContactsPhone + '\'' +
        "ptPostalCode='" + ptPostalCode + '\'' +
        "ptCreateCode='" + ptCreateCode + '\'' +
        "ptCreateDt='" + ptCreateDt + '\'' +
        "ptChild='" + ptChild + '\'' +
        "ptGuardianCardtype='" + ptGuardianCardtype + '\'' +
        "ptGuardianCardno='" + ptGuardianCardno + '\'' +
    '}';
    }

}
